/**
 * @file editor/ToGenerator.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy ToGenerator.
 */

#pragma once
#include <memory>
#include "../Visitor.h"
#include "../game/Pawn.h"
#include "../game/Bishop.h"
#include "../game/Rook.h"
#include "../game/Queen.h"
#include "../game/King.h"
#include "../generator/Pawn.h"
#include "../generator/Bishop.h"
#include "../generator/Rook.h"
#include "../generator/Queen.h"
#include "../generator/King.h"

namespace Editor
{
    class ToGenerator;
}

class Editor::ToGenerator: public Visitor
{
private:

    std::list<std::unique_ptr<Generator::Piece>> generator_pieces;

public:

    virtual void visit(const Game::Pawn& p);
    virtual void visit(const Game::Bishop& b);
    virtual void visit(const Game::Rook& r);
    virtual void visit(const Game::Queen& q);
    virtual void visit(const Game::King& k);

    std::list<std::unique_ptr<Generator::Piece>> getPieceList();
};
