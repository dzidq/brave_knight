/**
 * @file editor/CheckSolvable.cpp
 * @author Emil Bałdyga
 */

#include "CheckSolvable.h"
#include "ToGenerator.h"
#include "../generator/SolverBoard.h"
#include "../ExceptionQueueFull.h"
#include "../ExceptionQueueEmpty.h"

#include <iostream> // DEBUG

bool Editor::checkSolvable(const int8_t max_x, const int8_t max_y, const int8_t x, const int8_t y,
                           std::list<std::unique_ptr<Game::Piece>>& piece_list)
{
    Editor::ToGenerator tg_visitor;
    for (std::list<std::unique_ptr<Game::Piece>>::iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        (*it)->accept(tg_visitor);
    }
    std::list<std::unique_ptr<Generator::Piece>> unique_pieces(std::move(tg_visitor.getPieceList()));
    std::list<const Generator::Piece*> p_list;
    for (std::list<std::unique_ptr<Generator::Piece>>::iterator it = unique_pieces.begin(); it != unique_pieces.end(); ++it)
    {
        p_list.push_back(it->get());
    }
    Generator::SolverBoard sb(p_list, max_x, max_y);
    bool reachable = false;
    try
    {
        reachable = sb.allReachable(x, y);
    }
    catch (const ExceptionQueueFull& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const ExceptionQueueEmpty& ex)
    {
        std::cout << ex.what() << "\n";
    }
    return reachable;
}
