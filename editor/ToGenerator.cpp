/**
 * @file editor/ToGenerator.cpp
 * @author Emil Bałdyga
 */

#include "ToGenerator.h"

void Editor::ToGenerator::visit(const Game::Pawn& p)
{
    generator_pieces.push_back(std::make_unique<Generator::Pawn>(p.getX(), p.getY()));
}

void Editor::ToGenerator::visit(const Game::Bishop& b)
{
    generator_pieces.push_back(std::make_unique<Generator::Bishop>(b.getX(), b.getY()));
}

void Editor::ToGenerator::visit(const Game::Rook& r)
{
    generator_pieces.push_back(std::make_unique<Generator::Rook>(r.getX(), r.getY()));
}

void Editor::ToGenerator::visit(const Game::Queen& q)
{
    generator_pieces.push_back(std::make_unique<Generator::Queen>(q.getX(), q.getY()));
}

void Editor::ToGenerator::visit(const Game::King& k)
{
    generator_pieces.push_back(std::make_unique<Generator::King>(k.getX(), k.getY()));
}

std::list<std::unique_ptr<Generator::Piece>> Editor::ToGenerator::getPieceList()
{
    return std::move(generator_pieces);
}
