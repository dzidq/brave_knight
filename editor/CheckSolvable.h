/**
 * @file editor/CheckSolvable.h
 * @author Emil Bałdyga
 * @brief Plik zawierający funkcję do sprawdzania rozwiązywalności zadań szachowych.
 */

#pragma once
#include "../game/Piece.h"
#include <list>
#include <memory>

namespace Editor
{
    bool checkSolvable(const int8_t max_x, const int8_t max_y, const int8_t x, const int8_t y,
                       std::list<std::unique_ptr<Game::Piece>>& piece_list);
}
