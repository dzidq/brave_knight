/**
 * @file PieceType.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję typu Ptype.
 */

#pragma once
#include <cstdint>

constexpr uint8_t PIECE_TYPE_COUNT = 5; // Number of hostile pieces types

/**
 * @brief Typ wyliczeniowy określający typ figury.
 *
 * Typ ten jest używany podczas generacji plansz oraz w komunikacji z kodem interfejsu graficznego.
 */
enum class Ptype: uint8_t
{
    PAWN = 0,
    BISHOP,
    ROOK,
    QUEEN,
    KING
};
