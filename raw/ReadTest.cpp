#include "ReadTest.h"
#include "Files.h"
#include "../Types.h"
#include "../solver/Solver.h"
#include "../NodeAVL.hpp"
#include "../solver/Cache.h"
#include "../solver/CacheNode.h"
#include <vector>
#include <chrono>
#include <iostream>
#include "../Stats.hpp"

void readTest(const char* filename, const unsigned iterations)
{
    std::vector<unsigned> sol_time(iterations, 0);
    std::vector<unsigned> rotation_count(iterations, 0);
    std::vector<unsigned> insertion_count(iterations, 0);
    std::vector<unsigned> read_count(iterations, 0);
    std::list<Raw::Chessboard> raw_boards = loadRawBoardList(filename);
    for (unsigned i = 0; i < iterations; ++i)
    {
        for (std::list<Raw::Chessboard>::iterator it = raw_boards.begin(); it != raw_boards.end(); ++it)
        {
            std::list<std::unique_ptr<Game::Piece>> piece_list = it->getGamePieces();
            auto sol_start_time = std::chrono::system_clock::now();
            Solver::solve(it->getMaxX(), it->getMaxY(), it->getStartX(), it->getStartY(), std::move(piece_list));
            auto sol_end_time = std::chrono::system_clock::now();
            sol_time[i] += std::chrono::duration_cast<std::chrono::milliseconds>(sol_end_time-sol_start_time).count();
        }
        rotation_count[i] = NodeAVL<uint128_t, Solver::CacheNode>::rotation_count;
        insertion_count[i] = NodeAVL<uint128_t, Solver::CacheNode>::insertion_count;
        read_count[i] = Solver::Cache::read_count;
    }
    for (unsigned i = iterations-1; i > 0; --i)
    {
        rotation_count[i] -= rotation_count[i-1];
        insertion_count[i] -= insertion_count[i-1];
        read_count[i] -= read_count[i-1];
    }
    std::cout << "Solving time: (avg) " << mean(sol_time) << " ms\t(dev) " << stddev(sol_time) << " ms\n";
    std::cout << "AVL tree rotation count: (avg) " << mean(rotation_count) << "\t(dev) " << stddev(rotation_count) << std::endl;
    std::cout << "AVL tree insertion count: (avg) " << mean(insertion_count) << "\t(dev) " << stddev(insertion_count) << std::endl;
    if (NodeAVL<uint128_t, Solver::CacheNode>::insertion_count)
        std::cout << "Average AVL rotations to insertions ratio: " <<
            static_cast<double>(NodeAVL<uint128_t, Solver::CacheNode>::rotation_count)/
                NodeAVL<uint128_t, Solver::CacheNode>::insertion_count << std::endl;
    std::cout << "Read count: (avg) " << mean(read_count) << "\t(dev) " << stddev(read_count) << std::endl;
}

