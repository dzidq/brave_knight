#include "Chessboard.h"
#include "../game/ToRaw.h"

Raw::Chessboard::Chessboard(const int8_t _max_x, const int8_t _max_y, const int8_t _start_x, const int8_t _start_y,
const std::list<std::unique_ptr<Game::Piece>>& game_pieces):
max_x(_max_x), max_y(_max_y), start_x(_start_x), start_y(_start_y)
{
    Game::ToRaw tr_visitor;
    for (std::list<std::unique_ptr<Game::Piece>>::const_iterator it = game_pieces.begin(); it != game_pieces.end(); ++it)
    {
        (*it)->accept(tr_visitor);
    }
    piece_list = tr_visitor.getPieceList();
}

void Raw::Chessboard::save(std::stringstream& stream) const
{
    stream.write(reinterpret_cast<const char*>(&max_x), sizeof(max_x));
    stream.write(reinterpret_cast<const char*>(&max_y), sizeof(max_y));
    {
        const int8_t pos_num = start_x + max_x*start_y;
        stream.write(reinterpret_cast<const char*>(&pos_num), sizeof(pos_num));
    }
    {
        const int8_t size = static_cast<int8_t>(piece_list.size());
        stream.write(reinterpret_cast<const char*>(&size), sizeof(size));
    }
    for (std::list<Raw::Piece>::const_iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        stream.write(reinterpret_cast<const char*>(&(it->type)), sizeof(it->type));
        const int8_t pos_num = it->x + max_x*(it->y);
        stream.write(reinterpret_cast<const char*>(&(pos_num)), sizeof(pos_num));
    }
}

bool Raw::Chessboard::load(std::stringstream& stream)
{
    stream.read(reinterpret_cast<char*>(&max_x), sizeof(max_x));
    stream.read(reinterpret_cast<char*>(&max_y), sizeof(max_y));
    {
        int8_t pos_num;
        stream.read(reinterpret_cast<char*>(&pos_num), sizeof(pos_num));
        start_x = pos_num%max_x;
        start_y = pos_num/max_x;
    }
    int8_t size;
    stream.read(reinterpret_cast<char*>(&size), sizeof(size));
    Ptype type;
    int8_t pos_num;
    for (int8_t i = 0; i < size; ++i)
    {
        stream.read(reinterpret_cast<char*>(&type), sizeof(type));
        stream.read(reinterpret_cast<char*>(&pos_num), sizeof(pos_num));
        piece_list.push_back(Raw::Piece(type, pos_num%max_x, pos_num/max_x));
    }
    return stream.good();
}

int8_t Raw::Chessboard::getMaxX()
{
    return max_x;
}

int8_t Raw::Chessboard::getMaxY()
{
    return max_y;
}

int8_t Raw::Chessboard::getStartX()
{
    return start_x;
}

int8_t Raw::Chessboard::getStartY()
{
    return start_y;
}

std::list<std::unique_ptr<Game::Piece>> Raw::Chessboard::getGamePieces()
{
    std::list<std::unique_ptr<Game::Piece>> game_pieces;
    for (std::list<Raw::Piece>::const_iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        std::unique_ptr<Game::Piece> next_piece;
        switch (it->type)
        {
        case Ptype::PAWN: next_piece = std::make_unique<Game::Pawn>(it->x, it->y); break;
        case Ptype::BISHOP: next_piece = std::make_unique<Game::Bishop>(it->x, it->y); break;
        case Ptype::ROOK: next_piece = std::make_unique<Game::Rook>(it->x, it->y); break;
        case Ptype::QUEEN: next_piece = std::make_unique<Game::Queen>(it->x, it->y); break;
        case Ptype::KING: next_piece = std::make_unique<Game::King>(it->x, it->y); break;
        }
        game_pieces.push_back(std::move(next_piece));
    }
    return game_pieces;
}
