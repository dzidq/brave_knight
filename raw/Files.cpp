#include "Files.h"
#include <fstream>
#include <iostream>

void saveRawBoardList(const char* filename, const std::list<Raw::Chessboard>& board_list)
{
    std::stringstream stream;
    for (std::list<Raw::Chessboard>::const_iterator it = board_list.begin(); it != board_list.end(); ++it)
    {
        it->save(stream);
    }
    std::ofstream file_out(filename, std::ios::out|std::ios::binary);
    if (file_out.is_open())
    {
        file_out << stream.rdbuf();
        file_out.close();
    }
    else
    {
        std::cerr << "Failed to save " << filename << std::endl;
    }
}

std::list<Raw::Chessboard> loadRawBoardList(const char* filename)
{
    std::list<Raw::Chessboard> board_list;
    std::ifstream file_in(filename, std::ios::in|std::ios::binary);
    if (file_in.is_open())
    {
        std::stringstream stream;
        stream << file_in.rdbuf();
        while (true)
        {
            Raw::Chessboard board;
            if (!board.load(stream))
                break;
            board_list.push_back(board);
        }
        file_in.close();
    }
    else
    {
        std::cerr << "Failed to load " << filename << std::endl;
    }
    return board_list;
}
