#pragma once
#include "Piece.h"
#include <memory>
#include <cstdint>
#include <list>
#include "../game/Piece.h"
#include <sstream>

namespace Raw
{
    class Chessboard;
}

class Raw::Chessboard
{
private:

    int8_t max_x;
    int8_t max_y;
    int8_t start_x;
    int8_t start_y;
    std::list<Raw::Piece> piece_list;

public:

    Chessboard(const int8_t max_x, const int8_t max_y, const int8_t start_x, const int8_t start_y,
        const std::list<std::unique_ptr<Game::Piece>>& game_pieces);
    Chessboard(){} // Uninitialized

    void save(std::stringstream& stream) const;
    bool load(std::stringstream& stream);

    int8_t getMaxX();
    int8_t getMaxY();
    int8_t getStartX();
    int8_t getStartY();
    std::list<std::unique_ptr<Game::Piece>> getGamePieces();
};
