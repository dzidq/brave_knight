#pragma once
#include <list>
#include "Chessboard.h"

void saveRawBoardList(const char* filename, const std::list<Raw::Chessboard>& board_list);
std::list<Raw::Chessboard> loadRawBoardList(const char* filename);
