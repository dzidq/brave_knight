#pragma once
#include "../PieceType.h"
#include <cstdint>

namespace Raw
{
    class Piece;
}

class Raw::Piece
{
public:

    const Ptype type;
    const int8_t x;
    const int8_t y;

    Piece(const Ptype type, const int8_t x, const int8_t y);
};
