
/**
 * @file FixedQueue.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję szablonu klasy FixedQueue.
 */

#pragma once
#include <cstdint>
#include "ExceptionQueueFull.h"
#include "ExceptionQueueEmpty.h"

/**
 * @brief Kolejka FIFO o stałym rozmiarze.
 *
 * Rozmiar kolejki jest ustalany jednokrotnie przy konstrukcji danej instancji i może być różny
 * dla różnych obiektów, ale dla danego obiektu pozostaje stały do czasu jego zniszczenia.
 * Maksymalny rozmiar to 255 elementów (nie przewiduje się potrzeby na kolejki o większym rozmiarze
 * niż 100). Możliwe jest pobranie elementu tylko z początku kolejki i jest jednoznaczne z jego
 * usunięciem z kolejki. Analogicznie nowe elementy można wstawiać tylko na koniec kolejki.
 */
template <typename T>
class FixedQueue
{
private:

    T* queue; /**< Wektor elementów o rozmiarze @p fixed_size . */
    std::allocator<FixedQueue<T>> alloc;
    const uint8_t fixed_size; /**< Rozmiar kolejki. */
    uint8_t tail; /**< Koniec kolejki (tu wstawiane są nowe elementy). */
    uint8_t head; /**< Początek kolejki (stąd pobierane są elementy). */
    bool empty; /**< Flaga określająca czy kolejka jest pusta czy pełna, jeśli koniec i początek są równe. */

public:

    /**
     * @brief Konstruktor.
     *
     * Tworzy kolejkę o zadanym rozmiarze. Wszystkie elementy są inicjalizowane za pomocą konstruktora domyślnego.
     * @param size żądany rozmiar kolejki.
     */
    FixedQueue(const uint8_t size): fixed_size(size)
    {
        queue = new T[fixed_size];
        tail = 0;
        head = 0;
        empty = true;
    }

    /**
     * @brief Destruktor.
     */
    ~FixedQueue()
    {
        delete[] queue;
    }

    /**
     * @brief Wstawienie nowego elementu do kolejki.
     *
     * Wstawia pojedynczy element do kolejki, jeśli nie jest ona pełna. W przeciwnym przypadku metoda rzuca wyjątek.
     * @param e element do wstawienia.
     */
    void push(const T& e)
    {
        if (isFull())
        {
            throw ExceptionQueueFull();
        }
        queue[tail++] = e;
        if (tail == fixed_size)
        {
            tail = 0;
        }
        if (tail == head)
        {
            empty = false;
        }
    }

    /**
     * @brief Pobranie elementu z kolejki.
     *
     * Pobiera pojedynczy element z kolejki, jeśli nie jest ona pusta. W przeciwnym przypadku metoda rzuca wyjątek.
     * Pobranie elementu jednocześnie usuwa go z kolejki.
     */
    T pop()
    {
        if (isEmpty())
        {
            throw ExceptionQueueEmpty();
        }
        uint8_t old_head = head++;
        if (head == fixed_size)
        {
            head = 0;
        }
        if (tail == head)
        {
            empty = true;
        }
        return queue[old_head];
    }

    /**
     * @brief Sprawdzenie czy kolejka jest pusta.
     * @return Prawda, jeśli kolejka jest pusta. Fałsz w przeciwnym przypadku.
     */
    bool isEmpty() const
    {
        return (empty && tail == head);
    }

    /**
     * @brief Sprawdzenie czy kolejka jest pełna.
     * @return Prawda, jeśli kolejka jest pełna. Fałsz w przeciwnym przypadku.
     */
    bool isFull() const
    {
        return (!empty && tail == head);
    }
};
