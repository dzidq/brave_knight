#pragma once
#include <memory>
#include <utility>
#include <functional>
#include <vector>
#include <algorithm>

template <typename TK, typename TD>
class NodeAVL
{
public:

    /// Wskaźnik na unique_ptr samego siebie (analogiczny do "this").
    std::unique_ptr<NodeAVL<TK, TD>>* self;

    /// Klucz elementu w wierzchołku powiązany z danymi @p data .
    TK key;

    /// Dane znajdujące się w wierzchołku drzewa.
    TD data;

    /// Wskaźnik na lewe poddrzewo (o mniejszych wartościach kluczy).
    std::unique_ptr<NodeAVL<TK, TD>> left;

    /// Wskaźnik na prawe poddrzewo (o większych wartościach kluczy).
    std::unique_ptr<NodeAVL<TK, TD>> right;

    /// Współczynnik zbalansowania. Oznacza różnicę wysokości między prawym i lewym poddrzewem.
    int balance;

public:

    NodeAVL(std::unique_ptr<NodeAVL<TK, TD>>* const s, const TK k, const TD& d): self(s), key(k), data(d), balance(0){}

    NodeAVL(const NodeAVL& node, std::unique_ptr<NodeAVL<TK, TD>>* node_self): self(node_self), key(node.key), data(node.data)
    {
        if (node.left)
            left = std::make_unique<NodeAVL<TK, TD>>(*this, &left);
        if (node.right)
            right = std::make_unique<NodeAVL<TK, TD>>(*this, &right);
        balance = node.balance;
    }

    static unsigned rotation_count;

    static unsigned insertion_count;

    void rotateRight()
    {
        ++rotation_count;
        std::unique_ptr<NodeAVL<TK, TD>> temp_left = std::move(left);
        balance += 1 - std::min(0, temp_left->balance);
        temp_left->balance += 1 + std::max(0, balance);
        left = std::move(temp_left->right);
        if (left)
            left->self = &left;
        temp_left->right = std::move(*self);
        temp_left->self = self;
        *self = std::move(temp_left);
        self = &((*self)->right);
    }

    void rotateLeft()
    {
        ++rotation_count;
        std::unique_ptr<NodeAVL<TK, TD>> temp_right = std::move(right);
        balance += -1 - std::max(0, temp_right->balance);
        temp_right->balance += -1 + std::min(0, balance);
        right = std::move(temp_right->left);
        if (right)
            right->self = &right;
        temp_right->left = std::move(*self);
        temp_right->self = self;
        *self = std::move(temp_right);
        self = &((*self)->left);
    }

    std::tuple<bool, TD*, bool> insertFind(const TK k, const TD& d)
    {
        if (k == key) // Node found
        {
            return std::tuple<bool, TD*, bool>(true, &data, false);
        }
        std::tuple<bool, TD*, bool> ret;
        if (k < key)
        {
            if (left)
            {
                // Recursively check left node
                ret = left->insertFind(k, d);
                if (std::get<2>(ret)) // Check if balance was changed
                {
                    --balance;
                    if (balance != -1)
                    {
                        if (balance == -2) // Tree is unbalanced
                        {
                            if (left->balance == 1) // Left right rotation
                                left->rotateLeft();
                            rotateRight(); // Left left rotation
                        }
                        std::get<2>(ret) = false;
                    }
                }
            }
            else
            {
                // Insert new node (node not found)
                ++insertion_count;
                left = std::make_unique<NodeAVL<TK, TD>>(&left, k, d);
                --balance;
                ret = std::tuple<bool, TD*, bool>(false, &(left->data), static_cast<bool>(balance));
            }
            return ret;
        }
        // else
        if (right)
        {
            // Recursively check right node
            ret = right->insertFind(k, d);
            if (std::get<2>(ret)) // Check if balance was changed
            {
                ++balance;
                if (balance != 1)
                {
                    if (balance == 2) // Tree is unbalanced
                    {
                        if (right->balance == -1) // Right left rotation
                            right->rotateRight();
                        rotateLeft(); // Right Right rotation
                    }
                    std::get<2>(ret) = false;
                }
            }
        }
        else
        {
            // Insert new node (node not found)
            ++insertion_count;
            right = std::make_unique<NodeAVL<TK, TD>>(&right, k, d);
            ++balance;
            ret = std::tuple<bool, TD*, bool>(false, &(right->data), static_cast<bool>(balance));
        }
        return ret;
    }
};

template <typename TK, typename TD>
unsigned NodeAVL<TK, TD>::rotation_count = 0;

template <typename TK, typename TD>
unsigned NodeAVL<TK, TD>::insertion_count = 0;
