/**
 * @file generator/ExceptionSize.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionSize.h"

Generator::ExceptionSize::ExceptionSize(int8_t x, int8_t y): max_x(x), max_y(y){}

const char* Generator::ExceptionSize::what() const noexcept
{
    return "Incorrect chessboard size!";
}
