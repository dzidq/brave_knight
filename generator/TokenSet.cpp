#include "TokenSet.h"
#include "../RandGen.hpp"
#include <cstring>

#include <iostream> // DEBUG

Generator::TokenSet::TokenSet(const uint16_t (&density)[PIECE_TYPE_COUNT])
{
    int32_t bound = 0;
    for (int8_t i = 0; i < PIECE_TYPE_COUNT; ++i)
    {
        token_density[i] = density[i];
        bound += token_density[i];
        range[i] = bound;
        banned[i] = false;
    }
    tokens.resize(bound);
    size = bound;
    bound = 0;
    for (int8_t i = 0; i < PIECE_TYPE_COUNT; ++i)
    {
        for (uint16_t j = 0; j < token_density[i]; ++j, ++bound)
        {
            tokens[bound] = static_cast<Ptype>(i);
        }
    }
}

std::pair<bool, Ptype> Generator::TokenSet::getRandomPieceType()
{
    int32_t rand_pos = RandGen::get<int32_t>(0, size-1);
    for (int8_t i = 0; i < PIECE_TYPE_COUNT; ++i)
    {
        if (banned[i] || !token_density[i])
        {
            rand_pos += token_density[i];
        }
        else if (rand_pos < range[i])
        {
            return std::pair<bool, Ptype>(true, tokens[rand_pos]);
        }
    }
    return std::pair<bool, Ptype>(false, Ptype::PAWN);
}

void Generator::TokenSet::ban(Ptype type)
{
    banned[static_cast<int8_t>(type)] = true;
    size -= token_density[static_cast<int8_t>(type)];
}

void Generator::TokenSet::reset()
{
    memset(banned, false, sizeof(banned));
    size = tokens.size();
}
