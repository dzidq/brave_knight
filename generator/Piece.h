/**
 * @file generator/Piece.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::Piece.
 */

#pragma once
#include <list>
#include <vector>
#include "../Piece.h"
#include "../VisitElement.h"

namespace Game
{
    class Piece;
}

namespace Generator
{
    class Piece;
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
    class Chessboard;
    class SolverBoard;
}

class Generator::Piece: public ::Piece, public VisitElement
{
friend class Generator::Chessboard;
friend class Generator::Pawn;
friend class Generator::Bishop;
friend class Generator::Rook;
friend class Generator::Queen;
friend class Generator::King;
friend class Generator::SolverBoard;

protected:

    // Whether the piece was blocked during blockAttack function
    bool blocked;

    // Recursively add a list of pointers from "piece_board_list" and "this" to the chessboard
    void attack(Generator::Chessboard&) const;
    // Recursively add a constant list of pointers to the chessboard
    virtual void attack(Generator::Chessboard&, const std::vector<uint8_t>&) const = 0;
    // Recursively remove a constant list of pointers from the chessboard
    virtual void undoAttack(Generator::Chessboard&, const std::vector<uint8_t>&, const Generator::Piece* p) const = 0;
    // Remove piece pointers from the chessboard in positions after (p->x, p->y) field
    virtual void blockAttack(Generator::Chessboard&, const Generator::Piece* p) {};
    // Set board.reach_board to false in spots, where knight starts under attack
    // (piece position is temporarily set to knight position)
    virtual void reverseAttack(Generator::Chessboard&) const = 0;
    // Returns true if placing the piece in the chessboard would create a loop
    virtual bool checkLoop(Generator::Chessboard&) const = 0;
    // Mark fields that the piece can attack
    virtual void attack(Generator::SolverBoard&) const = 0;
    // Unmark fields that the piece can attack
    virtual void undoAttack(Generator::SolverBoard&) const = 0;

public:

    Piece(const int8_t x, const int8_t y);
    Piece(const Vector v);
    virtual ~Piece(){}

    virtual void accept(Visitor& v) = 0;
};
