/**
 * @file generator/SolverBoard.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::SolverBoard.
 */

#pragma once
#include <vector>
#include <list>
#include <cstdint>
#include "../FixedQueue.hpp"
#include "../Vector.h"

namespace Generator
{
    class Piece;
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
    class SolverBoard;
}

/**
 * @brief Klasa służąca do szybkiego sprawdzenia poprawności planszy.
 *
 * Podobnie jak klasa Solver::Chessboard służy do rozwiązywania zadań szachowych. W przeciwieństwie
 * do tamtej klasy, nie podaje dokładnego rozwiązania, a sprawdza jedynie czy takowe istnieje. Z
 * tego powodu może być używana bez obawy o długie opóźnienia. Używana jest podczas generacji
 * zagadek do częściowego sprawdzenia poprawności planszy oraz w edytorze.
 * @see Generator::Chessboard
 * @see Editor::checkSolvable()
 */
class Generator::SolverBoard
{
friend class Generator::Pawn;
friend class Generator::Bishop;
friend class Generator::Rook;
friend class Generator::Queen;
friend class Generator::King;

typedef std::vector<std::vector<const Generator::Piece*>> PiecePtr2d;
typedef std::vector<std::vector<bool>> Bool2d;
typedef std::vector<std::vector<int8_t>> Int8_2d;

private:

    const int8_t max_x;
    const int8_t max_y;
    std::list<const Generator::Piece*> piece_list; // List of piece pointers that are placed on the board
    PiecePtr2d piece_board; // Array of piece pointers
    Int8_2d attack_board; // Attacked positions (knight can't move there)
    Bool2d visited; // Positions already visited
    bool found_something;

    void clearVisited();

    // Action to be performed when knight reaches some field
    inline bool knightStep(const Generator::Piece* const p, const int8_t x, const int8_t y);
    // Recursively simulate knight moves; returns true if the piece "p" was reached
    bool simulateKnight(const Generator::Piece* const p, const int8_t x, const int8_t y);

    inline void onStep(uint8_t& count, const int8_t x, const int8_t y, FixedQueue<Vector>& field_queue);

    void simulateKnightStep(uint8_t& count, const int8_t x, const int8_t y, FixedQueue<Vector>& field_queue);

public:

    SolverBoard(const PiecePtr2d& p_board, std::list<const Generator::Piece*> p_list, const int8_t m_x, const int8_t m_y);
    SolverBoard(std::list<const Generator::Piece*> p_list, const int8_t m_x, const int8_t m_y);
    // Check whether the piece is reachable by knight from (x, y)
    bool isReachable(const Generator::Piece* const p, const int8_t x, const int8_t y);
    // Check whether all pieces are reachable by knight from (x, y)
    bool allReachable(const int8_t x, const int8_t y);
};
