#pragma once
#include <vector>
#include <utility>
#include "../PieceType.h"

namespace Generator
{
    class TokenSet;
}

class Generator::TokenSet
{
private:

    std::vector<Ptype> tokens;
    uint16_t token_density[PIECE_TYPE_COUNT];
    int32_t range[PIECE_TYPE_COUNT];
    bool banned[PIECE_TYPE_COUNT];
    int32_t size;

public:

    TokenSet(const uint16_t (&density)[PIECE_TYPE_COUNT]);

    std::pair<bool, Ptype> getRandomPieceType();
    void ban(Ptype type);
    void reset();
};
