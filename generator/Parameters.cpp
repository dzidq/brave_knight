#include "Parameters.h"
#include <vector>
#include <cmath>
#include "../RandGen.hpp"

Generator::Parameters::Parameters(const Generator::Parameters::PDF_MODE mode)
{
    min_board_size_x = min_board_size_y = 4;
    max_board_size_x = max_board_size_y = 10;
    knight_pos_x = -1;
    knight_pos_y = -1;
    min_pieces = 4;
    max_pieces = 16;
    max_token_position = 9.5;
    max_token_value = 4.5;
    min_token_value = 1.5;
    setTokens(mode);
}

void Generator::Parameters::setTokens(const Generator::Parameters::PDF_MODE mode)
{
    switch (mode)
    {
    case Generator::Parameters::PDF_MODE::CLASSIC:
        pawn_tokens = 8;
        bishop_tokens = 2;
        rook_tokens = 2;
        queen_tokens = 1;
        king_tokens = 1;
        break;
    case Generator::Parameters::PDF_MODE::EQUAL:
        pawn_tokens = 1;
        bishop_tokens = 1;
        rook_tokens = 1;
        queen_tokens = 1;
        king_tokens = 1;
        break;
    case Generator::Parameters::PDF_MODE::FULL:
        pawn_tokens = 1;
        bishop_tokens = 100;
        rook_tokens = 1000;
        queen_tokens = 10000;
        king_tokens = 10;
        break;
    case Generator::Parameters::PDF_MODE::PAWNS_ONLY:
        pawn_tokens = 1;
        bishop_tokens = 0;
        rook_tokens = 0;
        queen_tokens = 0;
        king_tokens = 0;
    }
}

void Generator::Parameters::setBoardSize(const int8_t board_size_x, const int8_t board_size_y)
{
    min_board_size_x = board_size_x;
    max_board_size_x = board_size_x;
    min_board_size_y = board_size_y;
    max_board_size_y = board_size_y;
}

void Generator::Parameters::setKnightPosition(const int8_t pos_x, const int8_t pos_y)
{
    knight_pos_x = pos_x;
    knight_pos_y = pos_y;
}

void Generator::Parameters::setPieceNumber(const uint8_t piece_num)
{
    min_pieces = piece_num;
    max_pieces = piece_num;
}

uint8_t Generator::Parameters::pickPieceNumber() const
{
    std::vector<uint16_t> distribution(max_pieces-min_pieces+1);

    float min_token_position; // Number of pieces to be given "min_ticket_value"
    if ((max_pieces+min_pieces)/2. > max_token_position)
    {
        min_token_position = max_pieces;
    }
    else
    {
        min_token_position = min_pieces;
    }
    // Polynomial coefficients
    float a = (min_token_value-max_token_value)/pow(max_token_position-min_token_position, 2);
    float b = -2*max_token_position*a;
    float c = min_token_value+min_token_position*a*(2*max_token_position-min_token_position);
    uint16_t dist_count = 0;
    for (uint8_t i = min_pieces; i <= max_pieces; ++i)
    {
        distribution[i-min_pieces] = static_cast<uint16_t>(a*i*i + b*i + c);
        dist_count += distribution[i-min_pieces];
    }
    std::vector<uint8_t> chances(dist_count);
    uint16_t count = 0;
    for (uint8_t i = 0; i < max_pieces-min_pieces+1; ++i)
    {
        for (uint16_t j = 0; j < distribution[i]; ++j)
        {
            chances[count] = i+min_pieces;
            ++count;
        }
    }
    return chances[RandGen::get(0, dist_count-1)];
}

Vector Generator::Parameters::pickBoardSize() const
{
    return Vector(RandGen::get(min_board_size_x, max_board_size_x), RandGen::get(min_board_size_y, max_board_size_y));
}

Vector Generator::Parameters::pickKnightPosition(const Vector board_size) const
{
    Vector pos(knight_pos_x, knight_pos_y);
    if (pos.x < 0)
        pos.x = RandGen::get(0, board_size.x-1);
    if (pos.y < 0)
        pos.y = RandGen::get(0, board_size.y-1);
    return pos;
}

void Generator::Parameters::bake()
{
    board_size = pickBoardSize();
    knight_start = pickKnightPosition(board_size);
    piece_num = pickPieceNumber();
}

uint8_t Generator::Parameters::getPieceNumber() const
{
    return piece_num;
}

Vector Generator::Parameters::getBoardSize() const
{
    return board_size;
}

Vector Generator::Parameters::getKnightPosition() const
{
    return knight_start;
}
