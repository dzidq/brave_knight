/**
 * @file generator/Queen.cpp
 * @author Emil Bałdyga
 */

#include "Queen.h"
#include "Chessboard.h"
#include "SolverBoard.h"
#include "../game/Queen.h"

Generator::Queen::Queen(const uint8_t x, const uint8_t y): Generator::Piece(x, y), Generator::Bishop(x, y), Generator::Rook(x, y){}

void Generator::Queen::accept(Visitor& v)
{
    v.visit(*this);
}

void Generator::Queen::attack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec) const
{
    Generator::Bishop::attack(board, piece_count_vec);
    Generator::Rook::attack(board, piece_count_vec);
}

void Generator::Queen::undoAttack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec, const Generator::Piece* p) const
{
    Generator::Bishop::undoAttack(board, piece_count_vec, p);
    Generator::Rook::undoAttack(board, piece_count_vec, p);
}

void Generator::Queen::blockAttack(Generator::Chessboard& board, const Generator::Piece* p)
{
    if (p->getX() == x || p->getY() == y)
    {
        return Generator::Rook::blockAttack(board, p);
    }
    return Generator::Bishop::blockAttack(board, p);
}

void Generator::Queen::reverseAttack(Generator::Chessboard& board) const
{
    Generator::Bishop::reverseAttack(board);
    Generator::Rook::reverseAttack(board);
}

bool Generator::Queen::checkLoop(Generator::Chessboard& board) const
{
    return Generator::Bishop::checkLoop(board) || Generator::Rook::checkLoop(board);
}

void Generator::Queen::attack(Generator::SolverBoard& s_board) const
{
    Generator::Bishop::attack(s_board);
    Generator::Rook::attack(s_board);
}

void Generator::Queen::undoAttack(Generator::SolverBoard& s_board) const
{
    Generator::Bishop::undoAttack(s_board);
    Generator::Rook::undoAttack(s_board);
}
