/**
 * @file generator/ExceptionSize.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::ExceptionSize.
 */

#pragma once
#include "Exception.h"
#include <cstdint>

namespace Generator
{
    class ExceptionSize;
}

class Generator::ExceptionSize: public Generator::Exception
{
private:

    const int8_t max_x;
    const int8_t max_y;

public:

    ExceptionSize(int8_t x, int8_t y);

    virtual const char* what() const noexcept;
};
