/**
 * @file generator/Generator.h
 * @author Emil Bałdyga
 * @brief Plik zawierający funkcję do generacji zadań szachowych.
 */

#pragma once
#include <memory>
#include <list>
#include "Parameters.h"

namespace Game
{
    class Piece;
}

namespace Generator
{
    /**
     * @brief Funkcja interfejsu do klas generujących zagadki szachowe.
     *
     * Funkcja stanowi zewnętrzny interfejs do łatwego generowania zadań szachowych. Zajmuje się
     * ona konwersją klas figur (Generator::Piece do Game::Piece) oraz zarządzaniem pamięcią
     * związaną z tą konwersją. Wewnętrznie do generacji jest wykorzystywana klasa
     * Generator::Chessboard. W przypadku, gdy z jakiegoś powodu nie da się wygenerować zadania,
     * rzucany jest wyjątek Generator::Exception.
     * @param params zestaw parametrów generacji.
     * @return Lista figur stojących na szachownicy.
     * @see Generator::Chessboard
     * @see Generator::Parameters
     * @see Generator::Piece
     * @see Game::Piece
     */
    std::list<std::unique_ptr<Game::Piece>> generate(const Generator::Parameters& params);
}
