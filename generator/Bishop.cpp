/**
 * @file generator/Bishop.cpp
 * @author Emil Bałdyga
 */

#include "Bishop.h"
#include "Chessboard.h"
#include "SolverBoard.h"
#include "../game/Bishop.h"

Generator::Bishop::Bishop(const uint8_t x, const uint8_t y): Generator::Piece(x, y){}

void Generator::Bishop::accept(Visitor& v)
{
    v.visit(*this);
}

void Generator::Bishop::attack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec) const
{
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= board.max_x || temp_y >= board.max_y)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= board.max_x || temp_y < 0)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= board.max_y)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            break;
        }
    }
}

void Generator::Bishop::undoAttack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec, const Generator::Piece* p) const
{
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= board.max_x || temp_y >= board.max_y)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= board.max_x || temp_y < 0)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= board.max_y)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
}

void Generator::Bishop::blockAttack(Generator::Chessboard& board, const Generator::Piece* p)
{
    std::vector<uint8_t> piece_count_vec = board.piece_board_count[x][y];
    piece_count_vec[board.piece_map.at(this)] = 1;
    bool found = false;
    int8_t temp_x, temp_y;
    temp_x = p->getX() - x;
    temp_y = p->getY() - y;
    if (temp_x == temp_y)
    {
        if (temp_x > 0) // RD
        {
            temp_x = x;
            temp_y = y;
            for (;;)
            {
                ++temp_x;
                ++temp_y;
                if (temp_x >= board.max_x || temp_y >= board.max_y)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
                    if (board.piece_board[temp_x][temp_y] != nullptr)
                    {
                        board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[temp_x][temp_y] != nullptr)
                {
                    if (board.piece_board[temp_x][temp_y] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
        else // LU
        {
            temp_x = x;
            temp_y = y;
            for (;;)
            {
                --temp_x;
                --temp_y;
                if (temp_x < 0 || temp_y < 0)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
                    if (board.piece_board[temp_x][temp_y] != nullptr)
                    {
                        board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[temp_x][temp_y] != nullptr)
                {
                    if (board.piece_board[temp_x][temp_y] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
    }
    else if (temp_x == -temp_y)
    {
        if (temp_x > 0) // RU
        {
            temp_x = x;
            temp_y = y;
            for (;;)
            {
                ++temp_x;
                --temp_y;
                if (temp_x >= board.max_x || temp_y < 0)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
                    if (board.piece_board[temp_x][temp_y] != nullptr)
                    {
                        board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[temp_x][temp_y] != nullptr)
                {
                    if (board.piece_board[temp_x][temp_y] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
        else // LD
        {
            temp_x = x;
            temp_y = y;
            for (;;)
            {
                --temp_x;
                ++temp_y;
                if (temp_x < 0 || temp_y >= board.max_y)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
                    if (board.piece_board[temp_x][temp_y] != nullptr)
                    {
                        board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[temp_x][temp_y] != nullptr)
                {
                    if (board.piece_board[temp_x][temp_y] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
    }
}

void Generator::Bishop::reverseAttack(Generator::Chessboard& board) const
{
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= board.max_x || temp_y >= board.max_y)
        {
            break;
        }
        board.reach_board[temp_x][temp_y] = false;
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= board.max_x || temp_y < 0)
        {
            break;
        }
        board.reach_board[temp_x][temp_y] = false;
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= board.max_y)
        {
            break;
        }
        board.reach_board[temp_x][temp_y] = false;
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0)
        {
            break;
        }
        board.reach_board[temp_x][temp_y] = false;
    }
}

bool Generator::Bishop::checkLoop(Generator::Chessboard& board) const
{
    const std::vector<uint8_t>& piece_count_vec = board.piece_board_count[x][y];
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= board.max_x || temp_y >= board.max_y)
        {
            break;
        }
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
            {
                return true;
            }
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= board.max_x || temp_y < 0)
        {
            break;
        }
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
            {
                return true;
            }
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= board.max_y)
        {
            break;
        }
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
            {
                return true;
            }
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0)
        {
            break;
        }
        if (board.piece_board[temp_x][temp_y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
            {
                return true;
            }
            break;
        }
    }
    return false;
}

void Generator::Bishop::attack(Generator::SolverBoard& s_board) const
{
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= s_board.max_x || temp_y >= s_board.max_y)
        {
            break;
        }
        ++s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= s_board.max_x || temp_y < 0)
        {
            break;
        }
        ++s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= s_board.max_y)
        {
            break;
        }
        ++s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0)
        {
            break;
        }
        ++s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
}

void Generator::Bishop::undoAttack(Generator::SolverBoard& s_board) const
{
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= s_board.max_x || temp_y >= s_board.max_y)
        {
            break;
        }
        --s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= s_board.max_x || temp_y < 0)
        {
            break;
        }
        --s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= s_board.max_y)
        {
            break;
        }
        --s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0)
        {
            break;
        }
        --s_board.attack_board[temp_x][temp_y];
        if (s_board.piece_board[temp_x][temp_y] != nullptr)
        {
            break;
        }
    }
}
