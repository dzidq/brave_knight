/**
 * @file generator/ToGame.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::ToGame.
 */

#pragma once
#include <memory>
#include "../Visitor.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"
#include "../game/Pawn.h"
#include "../game/Bishop.h"
#include "../game/Rook.h"
#include "../game/Queen.h"
#include "../game/King.h"

namespace Generator
{
    class ToGame;
}

class Generator::ToGame: public Visitor
{
private:

    std::list<std::unique_ptr<Game::Piece>> game_pieces;

public:

    virtual void visit(const Generator::Pawn& p);
    virtual void visit(const Generator::Bishop& b);
    virtual void visit(const Generator::Rook& r);
    virtual void visit(const Generator::Queen& q);
    virtual void visit(const Generator::King& k);

    std::list<std::unique_ptr<Game::Piece>> getPieceList();
};
