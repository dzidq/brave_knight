/**
 * @file generator/Piece.cpp
 * @author Emil Bałdyga
 */

#include "Piece.h"
#include "Chessboard.h"

Generator::Piece::Piece(const int8_t _x, const int8_t _y): ::Piece(_x, _y), blocked(false){}

Generator::Piece::Piece(const Vector v): ::Piece(v), blocked(false){}

void Generator::Piece::attack(Generator::Chessboard& board) const
{
    std::vector<uint8_t> piece_count_vec = board.piece_board_count[x][y];
    piece_count_vec[board.piece_map.at(this)] = 1;
    attack(board, piece_count_vec);
}
