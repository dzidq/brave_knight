/**
 * @file generator/Exception.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::Exception.
 */

#pragma once
#include <exception>

namespace Generator
{
    class Exception;
}

class Generator::Exception: public std::exception
{

};
