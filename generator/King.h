/**
 * @file generator/King.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::King.
 */

#pragma once
#include "Piece.h"

namespace Game
{
    class Piece;
}

namespace Generator
{
    class King;
}

class Generator::King: public Generator::Piece
{
protected:

    virtual void attack(Generator::Chessboard&, const std::vector<uint8_t>&) const;
    virtual void undoAttack(Generator::Chessboard&, const std::vector<uint8_t>&, const Generator::Piece* p) const;
    virtual void reverseAttack(Generator::Chessboard&) const;
    virtual bool checkLoop(Generator::Chessboard&) const;
    virtual void attack(Generator::SolverBoard&) const;
    virtual void undoAttack(Generator::SolverBoard&) const;

public:

    King(const uint8_t x, const uint8_t y);
    virtual void accept(Visitor& v);
};

