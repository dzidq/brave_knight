/**
 * @file generator/Chessboard.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::Chessboard.
 */

#pragma once
#include <list>
#include <vector>
#include <memory>
#include <unordered_map>
#include "Piece.h"
#include "TokenSet.h"
#include "Parameters.h"
#include "../PieceType.h"
#include "../Vector.h"

namespace Generator
{
    class Chessboard;
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
}

/**
 * @brief Klasa generująca zestawy figur szachowych.
 *
 * Jest odpowiedzialna za generację zadań szachowych z użyciem własnych metod oraz metod klasy
 * Generator::Piece. Z tego powodu jest ona zaprzyjaźniona z klasami pochodnymi klasy
 * Generator::Piece. Figury są dynamicznie alokowane przez szachownicę (tworzone są unique_ptr,
 * więc nie trzeba przejmować się ręcznym usuwaniem obiektów). Klasa potrafi stworzyć zestaw figur,
 * dla którego na pewno istnieje rozwiązanie. Do generacji zestawów figur należy używać zewnętrznej
 * funkcji Generator::generate().
 * @see Solver::Piece
 */
class Generator::Chessboard
{
friend class Generator::Piece;
friend class Generator::Pawn;
friend class Generator::Bishop;
friend class Generator::Rook;
friend class Generator::Queen;
friend class Generator::King;

typedef std::vector<std::vector<const Generator::Piece*>> PiecePtr2d;
typedef std::vector<std::vector<bool>> Bool2d;
typedef std::vector<std::vector<std::vector<uint8_t>>> Uint8_3d;

private:

    int8_t max_x; /**< Szerokość szachownicy */
    int8_t max_y; /**< Wysokość szachownicy */
    int8_t max_field; /**< Liczba pól szachownicy */
    int8_t start_x; /**< Pozycja początkowa skoczka w poziomie */
    int8_t start_y; /**< Pozycja początkowa skoczka w pionie */
    uint8_t max_pieces; /**< Maksymalna liczba bierek do wstawienia */

    /// Tablica wskaźników na figury szachowe.
    /** Ma wymiary @p max_x oraz @p max_y . Jeżeli na danym polu nie ma żadnej figury, przyjmuje
     *  ono wartość nullptr. */
    PiecePtr2d piece_board;
    // Fields that are currently reachable by a certain piece type
    /// Tablica flag oznaczających pozycje obecnie osiągalne przez dany typ figury.
    /** Ma wymiary @p max_x oraz @p max_y . Wartość true oznacza, że pole jest osiągalne. */
    Bool2d reach_board;
    // List of the generated pieces
    /// Lista sprytnych wskaźników na figury znajdujące się na szachownicy.
    std::list<std::unique_ptr<Generator::Piece>> piece_list;
    // List of raw pointers to the generated pieces
    /// Lista zwykłych wskaźników na figury znajdujące się na szachownicy.
    std::list<const Generator::Piece*> piece_ptr_list;

    // Remove this variable and create it on the fly?
    /// Obiekt określający dystrybuantę wyboru konkretnej figury do wstawienia.
    TokenSet token_set; // Chances that each piece will be chosen

    /// Mapa wskaźników na pionki na indeksy struktur związanych z generacją.
    std::unordered_map<const Generator::Piece*, uint8_t> piece_map;

    std::vector<Generator::Piece*> inverse_piece_map;

    Uint8_3d piece_board_count;

    uint8_t piece_count; // Current inserted piece count

    // Sets all fields of "reach_board" that don't contain a piece to true ((x, y) is the position of a knight)
    void clearReach(const int8_t x, const int8_t y);
    // Picks random field depending on availability (where "reach_board" is true)
    Vector pickField() const;

    void addToPieceBoardCount(const std::vector<uint8_t> piece_count_vec, const int8_t x, const int8_t y);

    void removeFromPieceBoardCount(const std::vector<uint8_t> piece_count_vec, const int8_t x, const int8_t y);

    // Call "blockAttack()" on each piece on piece_board_list only once
    void blockPieces(const Generator::Piece*);
    // Check whether piece can be reached from (x, y)
    bool isReachable(const Generator::Piece* const p, const int8_t x, const int8_t y);

public:

    Chessboard(const Generator::Parameters& params);

    std::list<std::unique_ptr<Generator::Piece>> generate();

    // DEBUG
    void drawReachBoard();
};
