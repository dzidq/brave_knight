/**
 * @file generator/SolverBoard.cpp
 * @author Emil Bałdyga
 */

#include "SolverBoard.h"
#include "Piece.h"

Generator::SolverBoard::SolverBoard(const PiecePtr2d& p_board, std::list<const Generator::Piece*> p_list, const int8_t m_x, const int8_t m_y):
max_x(m_x), max_y(m_y), attack_board(max_x, std::vector<int8_t>(max_y, 0)), visited(max_x, std::vector<bool>(max_y))
{
    piece_board = p_board;
    piece_list = p_list;
}

Generator::SolverBoard::SolverBoard(std::list<const Generator::Piece*> p_list, const int8_t m_x, const int8_t m_y):
max_x(m_x), max_y(m_y), piece_board(max_x, std::vector<const Generator::Piece*>(max_y, nullptr)),
attack_board(max_x, std::vector<int8_t>(max_y, 0)), visited(max_x, std::vector<bool>(max_y))
{
    piece_list = p_list;
    for (std::list<const Generator::Piece*>::iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        piece_board[(*it)->getX()][(*it)->getY()] = (*it);
    }
}

void Generator::SolverBoard::clearVisited()
{
    for (Bool2d::iterator it = visited.begin(); it != visited.end(); ++it)
    {
        std::fill(it->begin(), it->end(), false);
    }
}

bool Generator::SolverBoard::isReachable(const Generator::Piece* const p, const int8_t x, const int8_t y)
{
    for (std::list<const Generator::Piece*>::iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        (*it)->attack(*this);
    }
    do
    {
        found_something = false;
        clearVisited();
        if (simulateKnight(p, x, y))
        {
            return true;
        }
    } while (found_something);
    return false;
}

bool Generator::SolverBoard::knightStep(const Generator::Piece* const p, const int8_t x, const int8_t y)
{
    if (!attack_board[x][y] && !visited[x][y])
    {
        if (piece_board[x][y] != nullptr)
        {
            if (piece_board[x][y] == p) return true;
            found_something = true;
            piece_board[x][y]->undoAttack(*this);
            piece_board[x][y] = nullptr;
        }
        visited[x][y] = true;
        return simulateKnight(p, x, y);
    }
    return false;
}

// Possibly better performance with iterational implementation?
bool Generator::SolverBoard::simulateKnight(const Generator::Piece* const p, const int8_t x, const int8_t y)
{
    int8_t temp_x, temp_y;
    temp_x = x+2;
    if (temp_x < max_x)
    {
        temp_y = y+1;
        if (temp_y < max_y)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
        temp_y = y-1;
        if (temp_y >= 0)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
    }
    temp_x = x-2;
    if (temp_x >= 0)
    {
        temp_y = y+1;
        if (temp_y < max_y)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
        temp_y = y-1;
        if (temp_y >= 0)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
    }
    temp_y = y+2;
    if (temp_y < max_y)
    {
        temp_x = x+1;
        if (temp_x < max_x)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
        temp_x = x-1;
        if (temp_x >= 0)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
    }
    temp_y = y-2;
    if (temp_y >= 0)
    {
        temp_x = x+1;
        if (temp_x < max_x)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
        temp_x = x-1;
        if (temp_x >= 0)
        {
            if (knightStep(p, temp_x, temp_y)) return true;
        }
    }
    return false;
}

bool Generator::SolverBoard::allReachable(const int8_t x, const int8_t y)
{
    for (std::list<const Generator::Piece*>::iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        (*it)->attack(*this);
    }
    if (attack_board[x][y])
    {
        return false;
    }
    uint8_t count = piece_list.size();
    FixedQueue<Vector> field_queue(max_x*max_y);
    Vector temp;
    do
    {
        found_something = false;
        clearVisited();
        field_queue.push(Vector(x, y));
        while (!field_queue.isEmpty())
        {
            temp = field_queue.pop();
            simulateKnightStep(count, temp.x, temp.y, field_queue);
        }
    } while (found_something);

    if (count)
    {
        return false;
    }
    return true;
}

void Generator::SolverBoard::onStep(uint8_t& count, const int8_t x, const int8_t y, FixedQueue<Vector>& field_queue)
{
    if (!attack_board[x][y] && !visited[x][y])
    {
        if (piece_board[x][y] != nullptr)
        {
            found_something = true;
            --count;
            piece_board[x][y]->undoAttack(*this);
            piece_board[x][y] = nullptr;
        }
        visited[x][y] = true;
        field_queue.push(Vector(x, y));
    }
}

void Generator::SolverBoard::simulateKnightStep(uint8_t& count, const int8_t x, const int8_t y, FixedQueue<Vector>& field_queue)
{
    int8_t temp_x, temp_y;
    temp_x = x+2;
    if (temp_x < max_x)
    {
        temp_y = y+1;
        if (temp_y < max_y)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
        temp_y = y-1;
        if (temp_y >= 0)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
    }
    temp_x = x-2;
    if (temp_x >= 0)
    {
        temp_y = y+1;
        if (temp_y < max_y)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
        temp_y = y-1;
        if (temp_y >= 0)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
    }
    temp_y = y+2;
    if (temp_y < max_y)
    {
        temp_x = x+1;
        if (temp_x < max_x)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
        temp_x = x-1;
        if (temp_x >= 0)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
    }
    temp_y = y-2;
    if (temp_y >= 0)
    {
        temp_x = x+1;
        if (temp_x < max_x)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
        temp_x = x-1;
        if (temp_x >= 0)
        {
            onStep(count, temp_x, temp_y, field_queue);
        }
    }
}
