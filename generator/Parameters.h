#pragma once
#include "../Vector.h"

namespace Generator
{
    class Parameters;
}

class Generator::Parameters
{
private:

    uint8_t pickPieceNumber() const;
    Vector pickBoardSize() const;
    Vector pickKnightPosition(const Vector board_size) const;

    Vector board_size;
    Vector knight_start;
    uint8_t piece_num;

public:

    enum PDF_MODE // (pawns, bishops, rooks, queens, kings)
    {
        CLASSIC, // Default mode (8, 2, 2, 1, 1)
        EQUAL, // Equal probability (1, 1, 1, 1, 1)
        FULL, // Ordered probability (1, 100, 1000, 10000, 10)
        PAWNS_ONLY // Pawns only (1, 0, 0, 0, 0)
    };

    // Board size
    int8_t min_board_size_x; // Minimal width of the generated chessboard
    int8_t max_board_size_x; // Maximal width of the generated chessboard
    int8_t min_board_size_y; // Minimal height of the generated chessboard
    int8_t max_board_size_y; // Maximal height of the generated chessboard
    // Knight position
    int8_t knight_pos_x;
    int8_t knight_pos_y;
    // Number of pieces parameters
    uint8_t min_pieces; // Minimal number of pieces in the generated chessboard
    uint8_t max_pieces; // Maximal number of pieces in the generated chessboard
    float max_token_position; // Number of pieces to be given "max_token_value"
    float max_token_value; // Highest number of tokens given
    float min_token_value; // Lowest number of tokens given
    // Piece insertion PDF
    uint16_t pawn_tokens;
    uint16_t bishop_tokens;
    uint16_t rook_tokens;
    uint16_t queen_tokens;
    uint16_t king_tokens;

    Parameters(const Generator::Parameters::PDF_MODE mode = Generator::Parameters::PDF_MODE::CLASSIC);

    void setTokens(const Generator::Parameters::PDF_MODE mode);
    void setBoardSize(const int8_t board_size_x, const int8_t board_size_y);
    void setKnightPosition(const int8_t pos_x, const int8_t pos_y);
    void setPieceNumber(const uint8_t piece_num);

    void bake();

    uint8_t getPieceNumber() const;
    Vector getBoardSize() const;
    Vector getKnightPosition() const;
};
