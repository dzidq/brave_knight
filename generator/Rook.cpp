/**
 * @file generator/Rook.cpp
 * @author Emil Bałdyga
 */

#include "Rook.h"
#include "Chessboard.h"
#include "SolverBoard.h"
#include "../game/Rook.h"

Generator::Rook::Rook(const uint8_t x, const uint8_t y): Generator::Piece(x, y){}

void Generator::Rook::accept(Visitor& v)
{
    v.visit(*this);
}

void Generator::Rook::attack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec) const
{
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_x)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, temp, y);
        if (board.piece_board[temp][y] != nullptr)
        {
            board.piece_board[temp][y]->attack(board, piece_count_vec);
            break;
        }
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, temp, y);
        if (board.piece_board[temp][y] != nullptr)
        {
            board.piece_board[temp][y]->attack(board, piece_count_vec);
            break;
        }
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_y)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, x, temp);
        if (board.piece_board[x][temp] != nullptr)
        {
            board.piece_board[x][temp]->attack(board, piece_count_vec);
            break;
        }
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        board.addToPieceBoardCount(piece_count_vec, x, temp);
        if (board.piece_board[x][temp] != nullptr)
        {
            board.piece_board[x][temp]->attack(board, piece_count_vec);
            break;
        }
    }
}

void Generator::Rook::undoAttack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec, const Generator::Piece* p) const
{
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_x)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, temp, y);
        if (board.piece_board[temp][y] != nullptr)
        {
            if (board.piece_board[temp][y] != p)
            {
                board.piece_board[temp][y]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, temp, y);
        if (board.piece_board[temp][y] != nullptr)
        {
            if (board.piece_board[temp][y] != p)
            {
                board.piece_board[temp][y]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_y)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, x, temp);
        if (board.piece_board[x][temp] != nullptr)
        {
            if (board.piece_board[x][temp] != p)
            {
                board.piece_board[x][temp]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        board.removeFromPieceBoardCount(piece_count_vec, x, temp);
        if (board.piece_board[x][temp] != nullptr)
        {
            if (board.piece_board[x][temp] != p)
            {
                board.piece_board[x][temp]->undoAttack(board, piece_count_vec, p);
                break;
            }
            else if (blocked)
            {
                break;
            }
        }
    }
}

void Generator::Rook::blockAttack(Generator::Chessboard& board, const Generator::Piece* p)
{
    std::vector<uint8_t> piece_count_vec = board.piece_board_count[x][y];
    piece_count_vec[board.piece_map.at(this)] = 1;
    bool found = false;
    int8_t temp;
    if (p->getX() == x)
    {
        // DOWN
        if (p->getY() > y)
        {
            temp = y;
            for (;;)
            {
                ++temp;
                if (temp >= board.max_y)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, x, temp);
                    if (board.piece_board[x][temp] != nullptr)
                    {
                        board.piece_board[x][temp]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[x][temp] != nullptr)
                {
                    if (board.piece_board[x][temp] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
        // UP
        else
        {
            temp = y;
            for (;;)
            {
                --temp;
                if (temp < 0)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, x, temp);
                    if (board.piece_board[x][temp] != nullptr)
                    {
                        board.piece_board[x][temp]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[x][temp] != nullptr)
                {
                    if (board.piece_board[x][temp] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
    }
    else
    {
        // RIGHT
        if (p->getX() > x)
        {
            temp = x;
            for (;;)
            {
                ++temp;
                if (temp >= board.max_x)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, temp, y);
                    if (board.piece_board[temp][y] != nullptr)
                    {
                        board.piece_board[temp][y]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[temp][y] != nullptr)
                {
                    if (board.piece_board[temp][y] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
        // LEFT
        else
        {
            temp = x;
            for (;;)
            {
                --temp;
                if (temp < 0)
                {
                    break;
                }
                if (found)
                {
                    board.removeFromPieceBoardCount(piece_count_vec, temp, y);
                    if (board.piece_board[temp][y] != nullptr)
                    {
                        board.piece_board[temp][y]->undoAttack(board, piece_count_vec, p);
                        break;
                    }
                }
                else if (board.piece_board[temp][y] != nullptr)
                {
                    if (board.piece_board[temp][y] == p)
                    {
                        blocked = true;
                        found = true;
                    }
                    else
                        break;
                }
            }
        }
    }
}

void Generator::Rook::reverseAttack(Generator::Chessboard& board) const
{
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_x || board.piece_board[temp][y] != nullptr)
        {
            break;
        }
        board.reach_board[temp][y] = false;
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0 || board.piece_board[temp][y] != nullptr)
        {
            break;
        }
        board.reach_board[temp][y] = false;
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_y || board.piece_board[x][temp] != nullptr)
        {
            break;
        }
        board.reach_board[x][temp] = false;
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0 || board.piece_board[x][temp] != nullptr)
        {
            break;
        }
        board.reach_board[x][temp] = false;
    }
}

bool Generator::Rook::checkLoop(Generator::Chessboard& board) const
{
    const std::vector<uint8_t>& piece_count_vec = board.piece_board_count[x][y];
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_x)
        {
            break;
        }
        if (board.piece_board[temp][y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp][y])])
            {
                return true;
            }
            break;
        }
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        if (board.piece_board[temp][y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp][y])])
            {
                return true;
            }
            break;
        }
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_y)
        {
            break;
        }
        if (board.piece_board[x][temp] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[x][temp])])
            {
                return true;
            }
            break;
        }
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        if (board.piece_board[x][temp] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[x][temp])])
            {
                return true;
            }
            break;
        }
    }
    return false;
}

void Generator::Rook::attack(Generator::SolverBoard& s_board) const
{
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= s_board.max_x)
        {
            break;
        }
        ++s_board.attack_board[temp][y];
        if (s_board.piece_board[temp][y] != nullptr)
        {
            break;
        }
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        ++s_board.attack_board[temp][y];
        if (s_board.piece_board[temp][y] != nullptr)
        {
            break;
        }
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= s_board.max_y)
        {
            break;
        }
        ++s_board.attack_board[x][temp];
        if (s_board.piece_board[x][temp] != nullptr)
        {
            break;
        }
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        ++s_board.attack_board[x][temp];
        if (s_board.piece_board[x][temp] != nullptr)
        {
            break;
        }
    }
}

void Generator::Rook::undoAttack(Generator::SolverBoard& s_board) const
{
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= s_board.max_x)
        {
            break;
        }
        --s_board.attack_board[temp][y];
        if (s_board.piece_board[temp][y] != nullptr)
        {
            break;
        }
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        --s_board.attack_board[temp][y];
        if (s_board.piece_board[temp][y] != nullptr)
        {
            break;
        }
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= s_board.max_y)
        {
            break;
        }
        --s_board.attack_board[x][temp];
        if (s_board.piece_board[x][temp] != nullptr)
        {
            break;
        }
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0)
        {
            break;
        }
        --s_board.attack_board[x][temp];
        if (s_board.piece_board[x][temp] != nullptr)
        {
            break;
        }
    }
}
