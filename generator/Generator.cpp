/**
 * @file generator/Generator.cpp
 * @author Emil Bałdyga
 */

#include "Generator.h"
#include "Chessboard.h"
#include "ToGame.h"
#include "../game/Piece.h"
#include "../RandGen.hpp"
#include "ExceptionSize.h"
#include "ExceptionBounds.h"
#include "../ExceptionQueueFull.h"
#include "../ExceptionQueueEmpty.h"

#include <iostream> // DEBUG

extern const int8_t MIN_BOARD_SIZE; // Minimal chessboard size
extern const int8_t MAX_BOARD_SIZE; // Maximal chessboard size

std::list<std::unique_ptr<Game::Piece>> Generator::generate(const Generator::Parameters& params)
{
    Generator::ToGame tg_visitor;
    Generator::Chessboard board(params);
    try
    {
        std::list<std::unique_ptr<Generator::Piece>> generator_pieces = board.generate();
        for (std::list<std::unique_ptr<Generator::Piece>>::iterator it = generator_pieces.begin();
             it != generator_pieces.end(); ++it)
        {
            (*it)->accept(tg_visitor);
        }
    }
    catch (const Generator::ExceptionSize& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const Generator::ExceptionBounds& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const ExceptionQueueFull& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const ExceptionQueueEmpty& ex)
    {
        std::cout << ex.what() << "\n";
    }
    return tg_visitor.getPieceList();
}
