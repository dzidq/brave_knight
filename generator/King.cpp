/**
 * @file generator/King.cpp
 * @author Emil Bałdyga
 */

#include "King.h"
#include "Chessboard.h"
#include "SolverBoard.h"
#include "../game/King.h"

Generator::King::King(const uint8_t x, const uint8_t y): Generator::Piece(x, y){}

void Generator::King::accept(Visitor& v)
{
    v.visit(*this);
}

void Generator::King::attack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec) const
{
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            }
        }
        board.addToPieceBoardCount(piece_count_vec, x, temp_y);
        if (board.piece_board[x][temp_y] != nullptr)
        {
            board.piece_board[x][temp_y]->attack(board, piece_count_vec);
        }
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            }
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            }
        }
        board.addToPieceBoardCount(piece_count_vec, x, temp_y);
        if (board.piece_board[x][temp_y] != nullptr)
        {
            board.piece_board[x][temp_y]->attack(board, piece_count_vec);
        }
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            board.addToPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                board.piece_board[temp_x][temp_y]->attack(board, piece_count_vec);
            }
        }
    }
    temp_x = x+1;
    if (temp_x < board.max_x)
    {
        board.addToPieceBoardCount(piece_count_vec, temp_x, y);
        if (board.piece_board[temp_x][y] != nullptr)
        {
            board.piece_board[temp_x][y]->attack(board, piece_count_vec);
        }
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        board.addToPieceBoardCount(piece_count_vec, temp_x, y);
        if (board.piece_board[temp_x][y] != nullptr)
        {
            board.piece_board[temp_x][y]->attack(board, piece_count_vec);
        }
    }
}

void Generator::King::undoAttack(Generator::Chessboard& board, const std::vector<uint8_t>& piece_count_vec, const Generator::Piece* p) const
{
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr && board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
            }
        }
        board.removeFromPieceBoardCount(piece_count_vec, x, temp_y);
        if (board.piece_board[x][temp_y] != nullptr && board.piece_board[x][temp_y] != p)
        {
            board.piece_board[x][temp_y]->undoAttack(board, piece_count_vec, p);
        }
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr && board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
            }
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr && board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
            }
        }
        board.removeFromPieceBoardCount(piece_count_vec, x, temp_y);
        if (board.piece_board[x][temp_y] != nullptr && board.piece_board[x][temp_y] != p)
        {
            board.piece_board[x][temp_y]->undoAttack(board, piece_count_vec, p);
        }
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            board.removeFromPieceBoardCount(piece_count_vec, temp_x, temp_y);
            if (board.piece_board[temp_x][temp_y] != nullptr && board.piece_board[temp_x][temp_y] != p)
            {
                board.piece_board[temp_x][temp_y]->undoAttack(board, piece_count_vec, p);
            }
        }
    }
    temp_x = x+1;
    if (temp_x < board.max_x)
    {
        board.removeFromPieceBoardCount(piece_count_vec, temp_x, y);
        if (board.piece_board[temp_x][y] != nullptr && board.piece_board[temp_x][y] != p)
        {
            board.piece_board[temp_x][y]->undoAttack(board, piece_count_vec, p);
        }
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        board.removeFromPieceBoardCount(piece_count_vec, temp_x, y);
        if (board.piece_board[temp_x][y] != nullptr && board.piece_board[temp_x][y] != p)
        {
            board.piece_board[temp_x][y]->undoAttack(board, piece_count_vec, p);
        }
    }
}

void Generator::King::reverseAttack(Generator::Chessboard& board) const
{
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            board.reach_board[temp_x][temp_y] = false;
        }
        board.reach_board[x][temp_y] = false;
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            board.reach_board[temp_x][temp_y] = false;
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            board.reach_board[temp_x][temp_y] = false;
        }
        board.reach_board[x][temp_y] = false;
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            board.reach_board[temp_x][temp_y] = false;
        }
    }
    temp_x = x+1;
    if (temp_x < board.max_x)
    {
        board.reach_board[temp_x][y] = false;
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        board.reach_board[temp_x][y] = false;
    }
}

bool Generator::King::checkLoop(Generator::Chessboard& board) const
{
    const std::vector<uint8_t>& piece_count_vec = board.piece_board_count[x][y];
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
                {
                    return true;
                }
            }
        }
        if (board.piece_board[x][temp_y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[x][temp_y])])
            {
                return true;
            }
        }
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
                {
                    return true;
                }
            }
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
                {
                    return true;
                }
            }
        }
        if (board.piece_board[x][temp_y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[x][temp_y])])
            {
                return true;
            }
        }
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            if (board.piece_board[temp_x][temp_y] != nullptr)
            {
                if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][temp_y])])
                {
                    return true;
                }
            }
        }
    }
    temp_x = x+1;
    if (temp_x < board.max_x)
    {
        if (board.piece_board[temp_x][y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][y])])
            {
                return true;
            }
        }
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        if (board.piece_board[temp_x][y] != nullptr)
        {
            if (piece_count_vec[board.piece_map.at(board.piece_board[temp_x][y])])
            {
                return true;
            }
        }
    }
    return false;
}

void Generator::King::attack(Generator::SolverBoard& s_board) const
{
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < s_board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            ++s_board.attack_board[temp_x][temp_y];
        }
        ++s_board.attack_board[x][temp_y];
        temp_x = x+1;
        if (temp_x < s_board.max_x)
        {
            ++s_board.attack_board[temp_x][temp_y];
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            ++s_board.attack_board[temp_x][temp_y];
        }
        ++s_board.attack_board[x][temp_y];
        temp_x = x+1;
        if (temp_x < s_board.max_x)
        {
            ++s_board.attack_board[temp_x][temp_y];
        }
    }
    temp_x = x+1;
    if (temp_x < s_board.max_x)
    {
        ++s_board.attack_board[temp_x][y];
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        ++s_board.attack_board[temp_x][y];
    }
}

void Generator::King::undoAttack(Generator::SolverBoard& s_board) const
{
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < s_board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            --s_board.attack_board[temp_x][temp_y];
        }
        --s_board.attack_board[x][temp_y];
        temp_x = x+1;
        if (temp_x < s_board.max_x)
        {
            --s_board.attack_board[temp_x][temp_y];
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            --s_board.attack_board[temp_x][temp_y];
        }
        --s_board.attack_board[x][temp_y];
        temp_x = x+1;
        if (temp_x < s_board.max_x)
        {
            --s_board.attack_board[temp_x][temp_y];
        }
    }
    temp_x = x+1;
    if (temp_x < s_board.max_x)
    {
        --s_board.attack_board[temp_x][y];
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        --s_board.attack_board[temp_x][y];
    }
}
