/**
 * @file generator/Chessboard.cpp
 * @author Emil Bałdyga
 */

#include "Chessboard.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"
#include "SolverBoard.h"
#include "../RandGen.hpp"
#include <cmath>
#include "ExceptionSize.h"
#include "ExceptionBounds.h"

#include <iostream> // DEBUG

extern const int8_t MIN_BOARD_SIZE = 4; // Minimal chessboard size
extern const int8_t MAX_BOARD_SIZE = 10; // Maximal chessboard size

Generator::Chessboard::Chessboard(const Generator::Parameters& params):
token_set({params.pawn_tokens, params.bishop_tokens, params.rook_tokens, params.queen_tokens, params.king_tokens})
{
    const Vector board_size = params.getBoardSize();
    const Vector knight_pos = params.getKnightPosition();
    max_x = board_size.x;
    max_y = board_size.y;
    max_field = max_x*max_y;
    start_x = knight_pos.x;
    start_y = knight_pos.y;
    max_pieces = params.getPieceNumber();
    piece_board = PiecePtr2d(max_x, std::vector<const Generator::Piece*>(max_y, nullptr));
    reach_board = Bool2d(max_x, std::vector<bool>(max_y));
    piece_board_count = Uint8_3d(max_x, std::vector<std::vector<uint8_t>>(max_y, std::vector<uint8_t>(max_pieces, 0)));
    inverse_piece_map = std::vector<Generator::Piece*>(max_pieces);
    piece_count = 0;
}

void Generator::Chessboard::clearReach(const int8_t x, const int8_t y)
{
    for (Bool2d::iterator it = reach_board.begin(); it != reach_board.end(); ++it)
    {
        std::fill(it->begin(), it->end(), true);
    }
    for (std::list<std::unique_ptr<Generator::Piece>>::const_iterator it = piece_list.begin(); it != piece_list.end(); ++it)
    {
        reach_board[(*it)->x][(*it)->y] = false;
    }
    reach_board[x][y] = false;
}

Vector Generator::Chessboard::pickField() const
{
    Vector positions[max_field];
    int8_t pos_count = 0;
    for (int8_t x = 0; x < max_x; ++x)
    {
        for (int8_t y = 0; y < max_y; ++y)
        {
            if (reach_board[x][y])
            {
                positions[pos_count] = Vector(x, y);
                ++pos_count;
            }
        }
    }
    if (pos_count == 0)
    {
        return Vector(-1, -1);
    }
    return positions[RandGen::get(0, pos_count-1)];
}

void Generator::Chessboard::addToPieceBoardCount(const std::vector<uint8_t> piece_count_vec, const int8_t x, const int8_t y)
{
    for (uint8_t i = 0; i < piece_count; ++i)
    {
        piece_board_count[x][y][i] += piece_count_vec[i];
    }
}

void Generator::Chessboard::removeFromPieceBoardCount(const std::vector<uint8_t> piece_count_vec, const int8_t x, const int8_t y)
{
    for (uint8_t i = 0; i < piece_count; ++i)
    {
        piece_board_count[x][y][i] -= piece_count_vec[i];
    }
}

void Generator::Chessboard::blockPieces(const Generator::Piece* p)
{
    const std::vector<uint8_t>& piece_count_vec = piece_board_count[p->getX()][p->getY()];
    for (uint8_t i = 0; i < piece_count; ++i)
    {
        if (piece_count_vec[i])
        {
            inverse_piece_map[i]->blockAttack(*this, p);
        }
    }
    for (uint8_t i = 0; i < piece_count; ++i)
    {
        inverse_piece_map[i]->blocked = false;
    }
}

bool Generator::Chessboard::isReachable(const Generator::Piece* const p, const int8_t x, const int8_t y)
{
    piece_board[p->x][p->y] = p;
    piece_ptr_list.push_back(p);
    Generator::SolverBoard b(piece_board, piece_ptr_list, max_x, max_y);
    bool ret_val = b.isReachable(p, x, y);
    if (!ret_val)
    {
        piece_board[p->x][p->y] = nullptr;
        piece_ptr_list.pop_back();
    }
    return ret_val;
}

std::list<std::unique_ptr<Generator::Piece>> Generator::Chessboard::generate()
{
    const int8_t x = start_x;
    const int8_t y = start_y;
    if (max_x < MIN_BOARD_SIZE || max_x > MAX_BOARD_SIZE ||
        max_y < MIN_BOARD_SIZE || max_y > MAX_BOARD_SIZE)
    {
        throw Generator::ExceptionSize(max_x, max_y);
    }
    if (x < 0 || y < 0 || x >= max_x || y >= max_y)
    {
        throw Generator::ExceptionBounds(max_x, max_y, x, y);
    }

    /*std::list<std::unique_ptr<Generator::Piece>> dummy;
    for (int8_t xi = 0; xi < max_x; ++xi)
    {
        for (int8_t yi = 0; yi < max_y; ++yi)
        {
            if (!((xi == 0 && yi == 0) || ((xi == 0 || xi == 2 || xi == 4) && yi == 1)))
                dummy.push_back(std::make_unique<Generator::Pawn>(xi, yi));
        }
    }
    // Random permutation:
    {
        std::list<std::unique_ptr<Generator::Piece>>::iterator it1;
        std::list<std::unique_ptr<Generator::Piece>>::iterator it2;
        for (unsigned i = 0; i < 100; ++i)
        {
            it1 = dummy.begin();
            std::advance(it1, RandGen::get<unsigned>(0, dummy.size()-1));
            it2 = dummy.begin();
            std::advance(it2, RandGen::get<unsigned>(0, dummy.size()-1));
            std::iter_swap(it1, it2);
        }
    }
    return dummy;*/

    /*std::list<std::unique_ptr<Generator::Piece>> dummy;
    for (int8_t xi = 0; xi < max_x; ++xi)
    {
        for (int8_t yi = 0; yi < max_y; ++yi)
        {
            bool col = xi%2;
            if (col)
                dummy.push_back(std::make_unique<Generator::Pawn>(xi, yi));
        }
    }
    // Random permutation:
    {
        std::list<std::unique_ptr<Generator::Piece>>::iterator it1;
        std::list<std::unique_ptr<Generator::Piece>>::iterator it2;
        for (unsigned i = 0; i < 100; ++i)
        {
            it1 = dummy.begin();
            std::advance(it1, RandGen::get<unsigned>(0, dummy.size()-1));
            it2 = dummy.begin();
            std::advance(it2, RandGen::get<unsigned>(0, dummy.size()-1));
            std::iter_swap(it1, it2);
        }
    }
    return dummy;*/

    /*std::list<std::unique_ptr<Generator::Piece>> dummy;
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 0));
    return dummy;*/

    /*std::list<std::unique_ptr<Generator::Piece>> dummy;
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 5));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 1));
    return dummy;*/

    /*std::list<std::unique_ptr<Generator::Piece>> dummy;
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(4, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(4, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(4, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(4, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 3));
    return dummy;*/

    /*std::list<std::unique_ptr<Generator::Piece>> dummy;
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 6));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 7));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 6));
    dummy.push_back(std::make_unique<Generator::Rook>(5, 1));
    return dummy;*/

    /*dummy.push_back(std::make_unique<Generator::Pawn>(1, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 5));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 6));
    dummy.push_back(std::make_unique<Generator::Pawn>(1, 7));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 5));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 6));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 7));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 3));*/
    /*dummy.push_back(std::make_unique<Generator::Pawn>(5, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 5));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 6));
    dummy.push_back(std::make_unique<Generator::Pawn>(5, 7));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 4));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 5));
    dummy.push_back(std::make_unique<Generator::Pawn>(7, 6));*/

    /*dummy.push_back(std::make_unique<Generator::Rook>(3, 3));
    dummy.push_back(std::make_unique<Generator::Rook>(3, 1));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(3, 2));
    dummy.push_back(std::make_unique<Generator::Pawn>(0, 3));
    dummy.push_back(std::make_unique<Generator::Rook>(1, 3));
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 3));

    dummy.push_back(std::make_unique<Generator::Pawn>(0, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 0));
    dummy.push_back(std::make_unique<Generator::Pawn>(2, 1));*/

    //return dummy;

    /*drawReachBoard();

    std::unique_ptr<Generator::Piece> test;

    test = std::make_unique<Generator::Queen>(6, 4);
    piece_board[test->x][test->y] = nullptr;
    blockPieces(test->getPosition());
    piece_board[test->x][test->y] = test.get();
    test->attack(*this);
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    piece_list.push_back(std::move(test));

    drawReachBoard();

    test = std::make_unique<Generator::Rook>(5, 3);
    piece_board[test->x][test->y] = nullptr;
    blockPieces(test->getPosition());
    piece_board[test->x][test->y] = test.get();
    test->attack(*this);
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    piece_list.push_back(std::move(test));

    drawReachBoard();

    test = std::make_unique<Generator::Pawn>(6, 2);
    piece_board[test->x][test->y] = nullptr;
    blockPieces(test->getPosition());
    piece_board[test->x][test->y] = test.get();
    test->attack(*this);
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    piece_list.push_back(std::move(test));

    drawReachBoard();*/

    //return std::move(piece_list);

    /*std::unique_ptr<Generator::Piece> test;

    test = std::make_unique<Generator::Rook>(0, 2);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    test = std::make_unique<Generator::Pawn>(1, 2);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    test = std::make_unique<Generator::Rook>(2, 2);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    drawReachBoard();
    std::cout << std::endl;*/

    /*std::unique_ptr<Generator::Piece> test;

    test = std::make_unique<Generator::Rook>(3, 2);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    drawReachBoard();
    std::cout << std::endl;

    test = std::make_unique<Generator::Pawn>(4, 1);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    drawReachBoard();
    std::cout << std::endl;

    test = std::make_unique<Generator::Rook>(2, 1);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    drawReachBoard();
    std::cout << std::endl;

    test = std::make_unique<Generator::Pawn>(3, 1);
    piece_map[test.get()] = piece_count;
    inverse_piece_map[piece_count] = test.get();
    piece_board[test->x][test->y] = test.get();
    piece_ptr_list.push_back(test.get());
    ++piece_count;
    blockPieces(test.get());
    test->attack(*this);
    piece_list.push_back(std::move(test));

    drawReachBoard();
    std::cout << std::endl;*/


    // Real implementation

    Ptype next_piece_type;
    std::unique_ptr<Generator::Piece> next_piece;
    //std::cout << "max_pieces: " << max_pieces << std::endl;
    while (piece_count < max_pieces)
    {
        token_set.reset();
        bool no_positions;
        do // Try every piece type
        {
            {
                std::pair<bool, Ptype> new_piece_pair = token_set.getRandomPieceType();
                if (!new_piece_pair.first) // Couldn't fit any new pieces
                {
                    return std::move(piece_list);
                }
                next_piece_type = new_piece_pair.second;
            }
            switch (next_piece_type)
            {
            // Pieces are placed at (x, y) temporarily for algorithm purposes
            case Ptype::PAWN: next_piece = std::make_unique<Generator::Pawn>(x, y); break;
            case Ptype::BISHOP: next_piece = std::make_unique<Generator::Bishop>(x, y); break;
            case Ptype::ROOK: next_piece = std::make_unique<Generator::Rook>(x, y); break;
            case Ptype::QUEEN: next_piece = std::make_unique<Generator::Queen>(x, y); break;
            case Ptype::KING: next_piece = std::make_unique<Generator::King>(x, y); break;
            }
            // 1. Find mostly possible positions for the piece
            clearReach(x, y); // Mark standing pieces as inaccessible
            next_piece->reverseAttack(*this); // Reverse attack from the starting position
            // 2. Randomly choose one possible position
            no_positions = false;
            bool position_invalid;
            do // Try every position
            {
                position_invalid = false;
                next_piece->setPosition(pickField());
                if (next_piece->x == -1 || next_piece->y == -1)
                {
                    // 3. If no possible positions exist, modify "tickets" vector
                    token_set.ban(next_piece_type);
                    no_positions = true;
                    break;
                }
                // Check for circular dependencies
                // 4. Recursively check if the new piece is accessible from (x, y)
                if (next_piece->checkLoop(*this) || !isReachable(next_piece.get(), x, y))
                {
                    reach_board[next_piece->x][next_piece->y] = false;
                    position_invalid = true;
                }
            } while (position_invalid);
        } while (no_positions);
        // 5. Apply all postprocessing algorithms to the piece (if it's not the last one)
        const uint8_t old_piece_count = piece_count;
        ++piece_count;
        if (piece_count != max_pieces)
        {
            piece_map[next_piece.get()] = old_piece_count; // Add piece to map
            inverse_piece_map[old_piece_count] = next_piece.get();
            blockPieces(next_piece.get());
            next_piece->attack(*this);
        }
        piece_list.push_back(std::move(next_piece));
    }
    return std::move(piece_list);
}

void Generator::Chessboard::drawReachBoard()
{
    std::cout << "    ";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << " " << char('A'+i);
    }
    std::cout << std::endl;

    std::cout << "   +-";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << "--";
    }
    std::cout << "+" << std::endl;

    for (int8_t i = 0; i < max_y; ++i)
    {
        if (max_y-i < 10) std::cout << " ";
        std::cout << max_y-i;
        std::cout << " |";
        for (int8_t j = 0; j < max_x; ++j)
        {
            /*if (reach_board[j][i])
            {
                std::cout << "  ";
            }
            else
            {
                std::cout << " x";
            }*/
            uint16_t loop_piece_count = 0;
            for (uint8_t k = 0; k < piece_count; ++k)
            {
                loop_piece_count += piece_board_count[j][i][k];
            }
            std::cout << " " << static_cast<unsigned>(loop_piece_count);
        }
        std::cout << " | " << max_y-i << std::endl;
    }

    std::cout << "   +-";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << "--";
    }
    std::cout << "+" << std::endl;

    std::cout << "    ";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << " " << char('A'+i);
    }
    std::cout << std::endl;
}
