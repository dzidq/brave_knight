/**
 * @file generator/ToGame.cpp
 * @author Emil Bałdyga
 */

#include "ToGame.h"

void Generator::ToGame::visit(const Generator::Pawn& p)
{
    game_pieces.push_back(std::make_unique<Game::Pawn>(p.getX(), p.getY()));
}

void Generator::ToGame::visit(const Generator::Bishop& b)
{
    game_pieces.push_back(std::make_unique<Game::Bishop>(b.getX(), b.getY()));
}

void Generator::ToGame::visit(const Generator::Rook& r)
{
    game_pieces.push_back(std::make_unique<Game::Rook>(r.getX(), r.getY()));
}

void Generator::ToGame::visit(const Generator::Queen& q)
{
    game_pieces.push_back(std::make_unique<Game::Queen>(q.getX(), q.getY()));
}

void Generator::ToGame::visit(const Generator::King& k)
{
    game_pieces.push_back(std::make_unique<Game::King>(k.getX(), k.getY()));
}

std::list<std::unique_ptr<Game::Piece>> Generator::ToGame::getPieceList()
{
    return std::move(game_pieces);
}
