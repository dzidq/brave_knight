/**
 * @file generator/ExceptionBounds.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Generator::ExceptionBounds.
 */

#pragma once
#include "Exception.h"
#include <cstdint>

namespace Generator
{
    class ExceptionBounds;
}

class Generator::ExceptionBounds: public Generator::Exception
{
private:

    const int8_t max_x;
    const int8_t max_y;
    const int8_t x;
    const int8_t y;

public:

    ExceptionBounds(int8_t _max_x, int8_t _max_y, int8_t _x, int8_t _y);

    virtual const char* what() const noexcept;
};
