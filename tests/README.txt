Format:
[LiczbaTestów] _ [LiczbaFigur] [RodzajTestu] [TrybGeneracji] [WymiarPoziomy] x [WymiarPionowy]

Liczba figur "max" oznacza, że figury były wstawiane do momentu, gdy nie dało się już wstawić żadnej nowej figury (wstawiane "w opór"). Liczba figur w teście może być mniejsza niż podana, jeżeli nie udało się wstawić więcej figur.

Rodzaje testów:
Kolejne nazwy figur oznaczają rodzaje figur jakie znajdują się w teście. Nazwa "pieces" oznacza, że w teście występują wszystkie rodzaje figur.

Tryby generacji:
- full: figury wstawiane z następującym priorytetem: hetman, wieża, goniec, król, pion
- eq: figury wstawiane z jednakowym prawdopodobieństwem (brak możliwości wstawienia danej figury może wpłynąć na prawdopodobieństwo)
- cl: figury wstawiane z prawdopodobieństwami odpowiadającymi liczności figur w klasycznych szachach (brak podziału na gońce białopolowe i czarnopolowe) (brak możliwości wstawienia danej figury może wpłynąć na prawdopodobieństwo)
- [brak]: tryb generacji nie ma znaczenia
