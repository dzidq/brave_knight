#pragma once
#include "Types.h"

constexpr uint128_t HASH_MULT_HIGH = 0x72f6f3731bc0237d;
constexpr uint128_t HASH_MULT_LOW = 0xbf58476d1ce4e5b9;
constexpr uint128_t HASH_MULT = (HASH_MULT_HIGH << 64) | HASH_MULT_LOW;

constexpr uint128_t HASH_ADD_HIGH = 0x94d049bb133111eb;
constexpr uint128_t HASH_ADD_LOW = 0xaf17449a83681de2;
constexpr uint128_t HASH_ADD = (HASH_ADD_HIGH << 64) | HASH_ADD_LOW;

uint128_t perfectHash(uint128_t val)
{
    val = ((val*HASH_MULT) + HASH_ADD);
    val ^= (val >> 63);
    val ^= (val >> 60);
    val ^= (val >> 33);
    return val;
}
