/**
 * @file solver/Chessboard.cpp
 * @author Emil Bałdyga
 */

#include "Chessboard.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"
#include "ExceptionLoop.h"
#include "ExceptionBounds.h"
#include "ExceptionAttack.h"
#include "ExceptionUnsolvable.h"
#include "ToGenerator.h"
#include "../generator/SolverBoard.h"
#include "../RandGen.hpp"
#include <limits>
#include <algorithm>
#include <cmath>

#include <iostream> // DEBUG

Solver::Chessboard::Chessboard(const int8_t _max_x, const int8_t _max_y, const std::list<std::unique_ptr<Solver::Piece>>& pieces):
max_x(_max_x), max_y(_max_y), max_field(max_x*max_y),
piece_board(max_x, std::vector<Solver::Piece*>(max_y, nullptr)), attack_board(max_x, std::vector<int8_t>(max_y, 0))
{
    piece_vector.resize(pieces.size());
    Solver::Piece* ptr;
    color_shift = 0;
    size_t i = 0;
    for (std::list<std::unique_ptr<Solver::Piece>>::const_iterator it = pieces.begin(); it != pieces.end(); ++it, ++i)
    {
        ptr = it->get();
        piece_vector[i] = ptr;
        piece_board[ptr->x][ptr->y] = ptr;
        if ((ptr->getX()+ptr->getY())%2) // Is field white?
            --color_shift; // White field
        else
            ++color_shift; // Black field
    }
}

Solver::Chessboard::Chessboard(const Chessboard& board, Solver::Piece* omit):
    max_x(board.max_x), max_y(board.max_y), max_field(max_x*max_y),
    piece_board(board.piece_board), attack_board(board.attack_board)
{
    piece_vector.resize(board.piece_vector.size()-1);
    size_t i = 0;
    for (std::vector<Solver::Piece*>::const_iterator it = board.piece_vector.begin(); it != board.piece_vector.end(); ++it)
    {
        if (*it != omit)
        {
            piece_vector[i] = *it;
            ++i;
        }
    }
    on_white = omit->onWhite();
    omit->undoAttack(*this);
    piece_board[omit->x][omit->y] = nullptr;
    color_shift = board.color_shift + static_cast<int8_t>(on_white)*2 - 1;
}

void Solver::Chessboard::initializeGraph()
{
    for (std::vector<Solver::Piece*>::iterator it = piece_vector.begin(); it != piece_vector.end(); ++it)
    {
       (*it)->attack(*this);
    }
}

uint16_t Solver::Chessboard::solve(const int8_t x, const int8_t y)
{
    initializeGraph();
    std::cout << "\n";
    drawMergedA();
    std::cout << "\n";
    if (hasLoops())
    {
        throw Solver::ExceptionLoop();
    }
    std::cout << "Starting position: (" << static_cast<uint16_t>(x) << ";" << static_cast<uint16_t>(y) << ")\n\n";
    if (x < 0 || y < 0 || x >= max_x || y >= max_y)
    {
        throw Solver::ExceptionBounds(max_x, max_y, x, y);
    }
    if (attack_board[x][y])
    {
        throw Solver::ExceptionAttack(x, y);
    }
    if (!allReachable(x, y))
    {
        throw Solver::ExceptionUnsolvable();
    }
    randPermute();
    uint16_t limit = std::numeric_limits<uint16_t>::max();
    History history;
    Cache positions(max_x, max_y, piece_vector, x, y);
    on_white = (x+y)%2;
    split(x, y, 0, limit, positions, history);
    if (limit == std::numeric_limits<uint16_t>::max())
    {
        throw Solver::ExceptionUnsolvable();
    }
    std::cout << "\nSolution: " << limit << " (is that right?)\n\n";

    //history.print();

    std::vector<Vector> solution = intermediateSteps(x, y, history.getCoupling(), limit);
    for (size_t i = 0; i < solution.size(); ++i)
    {
        std::cout << "(" << (int)solution[i].x << "; " << (int)solution[i].y << ")\n";
    }
    std::cout << std::endl;

    return limit;
}

uint16_t Solver::Chessboard::split(const int8_t x, const int8_t y, const uint16_t weight, uint16_t& limit,
    Cache& positions, History& history)
{
    if (piece_vector.empty())
    {
        if (weight < limit)
        {
            std::cout << "[END] Limit was updated to " << weight << "\n";
            limit = weight;
        }
        else
        {
            return std::numeric_limits<uint16_t>::max();
        }
        history.setCoupling(nullptr);
        return 0;
    }
    {
        bool extra_step;
        if (on_white)
            extra_step = color_shift < 0;
        else
            extra_step = color_shift > 0;
        // Distance heuristics
        if (weight + piece_vector.size() + std::max(0, abs(color_shift)-1) + static_cast<uint16_t>(extra_step) >= limit)
        {
            return std::numeric_limits<uint16_t>::max(); // Special solution value
        }
    }
    const std::pair<bool, CacheNode*> node_ptr_pair = positions.insertFind(x, y, piece_vector, CacheNode(weight));
    if (node_ptr_pair.first) // Position was cached
    {
        const uint16_t part_solution = (node_ptr_pair.second)->solution;
        if ((node_ptr_pair.second)->solution != std::numeric_limits<uint16_t>::max())
        {
            const uint16_t pos_limit = weight + part_solution;
            if (pos_limit < limit)
            {
                std::cout << "[CACHE] Limit was updated to " << pos_limit << "\n";
                limit = pos_limit;
                history.setCoupling(&((node_ptr_pair.second)->h_elem));
                return part_solution;
            }
            else
            {
                return std::numeric_limits<uint16_t>::max(); // Special solution value
            }
        }
        else if (weight >= (node_ptr_pair.second)->steps)
        {
            return std::numeric_limits<uint16_t>::max(); // Special solution value
        }
        (node_ptr_pair.second)->steps = weight;
    }

    std::list<Solver::Piece*> roots = getRoots();
    Int8_2d weights;
    calculateWeights(x, y, weights);
    sortCutByWeight(roots, weights);

    int8_t temp_weight;
    CacheNode temp_node(weight);
    temp_node.solution = std::numeric_limits<uint16_t>::max();
    Solver::Piece* p_ptr;

    for (std::list<Solver::Piece*>::iterator it = roots.begin(); it != roots.end(); ++it)
    {
        p_ptr = *it;
        temp_weight = weights[p_ptr->x][p_ptr->y];
        Chessboard board(*this, p_ptr); // Performs shallow copy!!!
        uint16_t part_solution = board.split(p_ptr->x, p_ptr->y,
            weight+static_cast<uint16_t>(temp_weight), limit, positions, history);
        if (part_solution != std::numeric_limits<uint16_t>::max())
        {
            part_solution += static_cast<uint16_t>(temp_weight);
        }
        if (part_solution < temp_node.solution)
        {
            temp_node.h_elem.next = history.getCoupling();
            temp_node.h_elem.position = Vector(p_ptr->x, p_ptr->y);
            temp_node.solution = part_solution;
        }
    }

    if (temp_node.solution != std::numeric_limits<uint16_t>::max())
    {
        *(node_ptr_pair.second) = temp_node;
        history.setCoupling(&((node_ptr_pair.second)->h_elem));
    }

    return temp_node.solution;
}

bool Solver::Chessboard::hasLoops()
{
    std::list<Solver::Piece*> reached;
    for (std::vector<Solver::Piece*>::iterator it = piece_vector.begin(); it != piece_vector.end(); ++it)
    {
        if (hasLoopsDFS(reached, *it))
        {
            return true;
        }
    }
    return false;
}

bool Solver::Chessboard::hasLoopsDFS(std::list<Solver::Piece*>& reached, Solver::Piece* p)
{
    if (p->wasProcessed())
    {
        return false;
    }
    // Assuming that any piece cannot defend itself
    for (std::list<Solver::Piece*>::iterator it = reached.begin(); it != reached.end(); ++it)
    {
        if (*it == p)
        {
            return true;
        }
    }
    reached.push_back(p);
    for (std::list<Solver::Piece*>::iterator it = p->defense.begin(); it != p->defense.end(); ++it)
    {
        if (hasLoopsDFS(reached, *it))
        {
            return true;
        }
    }
    p->process();
    reached.pop_back();
    return false;
}

std::list<Solver::Piece*> Solver::Chessboard::getRoots()
{
    std::list<Solver::Piece*> roots;
    for (std::vector<Solver::Piece*>::iterator it = piece_vector.begin(); it != piece_vector.end(); ++it)
    {
        if ((*it)->defense.empty())
        {
            roots.push_back(*it);
        }
    }
    return roots;
}

void Solver::Chessboard::calculateWeights(const int8_t x, const int8_t y, Int8_2d& weights)
{
    weights.resize(max_x, std::vector<int8_t>(max_y, max_field)); // Set maximal value
    weights[x][y] = 0;
    FixedQueue<Vector> field_queue(max_field);
    field_queue.push(Vector(x, y));
    Vector temp;
    while (!field_queue.isEmpty())
    {
        temp = field_queue.pop();
        markWeights(temp.x, temp.y, weights, field_queue);
    }
}

void Solver::Chessboard::setWeight(const int8_t x, const int8_t y, int8_t& weight, const int8_t value, FixedQueue<Vector>& field_queue)
{
    if (!attack_board[x][y] && weight > value)
    {
        weight = value;
        // There's no point in marking pieces that require taking another pieces to reach (won't be the optimal solution)
        if (piece_board[x][y] == nullptr)
        {
            field_queue.push(Vector(x, y));
        }
    }
}

void Solver::Chessboard::markWeights(const int8_t x, const int8_t y, Int8_2d& weights, FixedQueue<Vector>& field_queue)
{
    int8_t value = weights[x][y] + 1;
    int8_t temp_x, temp_y;
    temp_x = x+2;
    if (temp_x < max_x)
    {
        temp_y = y+1;
        if (temp_y < max_y)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
        temp_y = y-1;
        if (temp_y >= 0)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
    }
    temp_x = x-2;
    if (temp_x >= 0)
    {
        temp_y = y+1;
        if (temp_y < max_y)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
        temp_y = y-1;
        if (temp_y >= 0)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
    }
    temp_y = y+2;
    if (temp_y < max_y)
    {
        temp_x = x+1;
        if (temp_x < max_x)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
        temp_x = x-1;
        if (temp_x >= 0)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
    }
    temp_y = y-2;
    if (temp_y >= 0)
    {
        temp_x = x+1;
        if (temp_x < max_x)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
        temp_x = x-1;
        if (temp_x >= 0)
        {
            setWeight(temp_x, temp_y, weights[temp_x][temp_y], value, field_queue);
        }
    }
}

void Solver::Chessboard::sortCutByWeight(std::list<Solver::Piece*>& pieces, const Int8_2d& weights)
{
    std::list<float> weight_list;
    for (std::list<Solver::Piece*>::iterator it = pieces.begin(); it != pieces.end();)
    {
        int8_t weight = weights[(*it)->x][(*it)->y];
        if (weight == max_field)
            it = pieces.erase(it);
        else
        {
            if ((color_shift > 0 && on_white && (*it)->onWhite()) || (color_shift < 0 && !on_white && !(*it)->onWhite()))
                ++weight;
            float temp_weight = static_cast<float>(weight);
            temp_weight -= static_cast<float>((*it)->getAttackFieldCount())/max_field;
            weight_list.push_back(temp_weight);
            ++it;
        }
    }
    float temp_weight;
    float min_weight;
    std::list<Solver::Piece*>::iterator min_it;
    std::list<Solver::Piece*>::iterator swap_it;
    const std::list<Solver::Piece*>::iterator sort_end_it = --pieces.end();
    std::list<float>::iterator min_w_it;
    std::list<float>::iterator swap_w_it;
    std::list<float>::iterator curr_w_it = weight_list.begin();
    for (std::list<Solver::Piece*>::iterator curr_it = pieces.begin(); curr_it != sort_end_it;)
    {
        min_weight = *curr_w_it;
        min_it = curr_it;
        swap_it = curr_it;
        min_w_it = curr_w_it;
        swap_w_it = curr_w_it;
        ++curr_w_it;
        ++curr_it;
        std::list<float>::iterator w_it = curr_w_it;
        for (std::list<Solver::Piece*>::iterator it = curr_it; it != pieces.end(); ++it, ++w_it)
        {
            temp_weight = *w_it;
            if (temp_weight < min_weight)
            {
                min_weight = temp_weight;
                min_w_it = w_it;
                min_it = it;
            }
        }
        std::iter_swap(min_w_it, swap_w_it);
        std::iter_swap(min_it, swap_it);
    }
}

bool Solver::Chessboard::allReachable(const int8_t x, const int8_t y)
{
    Solver::ToGenerator tg_visitor;
    for (std::vector<Solver::Piece*>::iterator it = piece_vector.begin(); it != piece_vector.end(); ++it)
    {
        (*it)->accept(tg_visitor);
    }
    std::list<std::unique_ptr<Generator::Piece>> gen_pieces = std::move(tg_visitor.getPieceList());
    std::list<const Generator::Piece*> gen_ptr_list;
    for (std::list<std::unique_ptr<Generator::Piece>>::iterator it = gen_pieces.begin(); it != gen_pieces.end(); ++it)
    {
        gen_ptr_list.push_back(it->get());
    }
    Generator::SolverBoard solver_board(gen_ptr_list, max_x, max_y);
    return solver_board.allReachable(x, y);
}

void Solver::Chessboard::randPermute()
{
    if (!piece_vector.empty())
    {
        size_t i;
        size_t j;
        const size_t max_i = piece_vector.size()-1;
        for (i = 0; i < max_i; ++i)
        {
            j = RandGen::get<size_t>(i, max_i);
            std::swap(piece_vector[i], piece_vector[j]);
        }
    }
}

std::vector<Vector> Solver::Chessboard::intermediateSteps(const int8_t x, const int8_t y,
    const HistoryElement* step_list, const uint16_t limit)
{
    std::vector<Vector> solution(limit);
    Vector target(x, y);
    Vector start;
    Vector step;
    const HistoryElement* curr_elem = step_list;
    Int8_2d weights;
    weights.resize(max_x, std::vector<int8_t>(max_y, max_field));
    uint16_t step_num = 0;
    while (curr_elem)
    {
        for (int8_t i = 0; i < max_x; ++i)
        {
            std::fill(weights[i].begin(), weights[i].end(), max_field);
        }
        calculateWeights(target.x, target.y, weights);
        start = curr_elem->position;
        step_num += weights[start.x][start.y];
        solution[step_num-1] = start;
        step = start;
        for (int8_t w = weights[start.x][start.y]-1, k = 2; w > 0; --w, ++k)
        {
            int8_t temp_x, temp_y;
            temp_x = step.x+2;
            if (temp_x < max_x)
            {
                temp_y = step.y+1;
                if (temp_y < max_y && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
                temp_y = step.y-1;
                if (temp_y >= 0 && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
            }
            temp_x = step.x-2;
            if (temp_x >= 0)
            {
                temp_y = step.y+1;
                if (temp_y < max_y && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
                temp_y = step.y-1;
                if (temp_y >= 0 && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
            }
            temp_y = step.y+2;
            if (temp_y < max_y)
            {
                temp_x = step.x+1;
                if (temp_x < max_x && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
                temp_x = step.x-1;
                if (temp_x >= 0 && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
            }
            temp_y = step.y-2;
            if (temp_y >= 0)
            {
                temp_x = step.x+1;
                if (temp_x < max_x && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
                temp_x = step.x-1;
                if (temp_x >= 0 && weights[temp_x][temp_y] == w)
                {
                    step = Vector(temp_x, temp_y);
                    solution[step_num-k] = step;
                    continue;
                }
            }
        }
        target = start;
        piece_board[start.x][start.y]->undoAttack(*this);
        piece_board[start.x][start.y] = nullptr;
        curr_elem = curr_elem->next;
    }
    return std::move(solution);
}

void Solver::Chessboard::drawMergedA()
{
    std::cout << "    ";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << " " << char('A'+i);
    }
    std::cout << "\n";

    std::cout << "   +-";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << "--";
    }
    std::cout << "+\n";

    for (int8_t i = 0; i < max_y; ++i)
    {
        if (max_y-i < 10) std::cout << " ";
        std::cout << max_y-i;
        std::cout << " |";
        for (int8_t j = 0; j < max_x; ++j)
        {
            Solver::Piece* p = piece_board[j][i];
            if (p == nullptr)
            {
                if (!attack_board[j][i])
                {
                    std::cout << "  ";
                }
                else
                {
                    std::cout << " x";
                }
            }
            else
            {
                std::cout << ' ' << p->getSign();
            }
        }
        std::cout << " | " << max_y-i << "\n";
    }

    std::cout << "   +-";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << "--";
    }
    std::cout << "+\n";

    std::cout << "    ";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << " " << char('A'+i);
    }
    std::cout << "\n";
}

void Solver::Chessboard::drawMergedB()
{
    std::cout << "    ";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << " " << char('A'+i);
    }
    std::cout << "\n";

    std::cout << "   +-";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << "--";
    }
    std::cout << "+\n";

    for (int8_t i = 0; i < max_y; ++i)
    {
        if (max_y-i < 10) std::cout << " ";
        std::cout << max_y-i;
        std::cout << " |";
        for (int8_t j = 0; j < max_x; ++j)
        {
            Solver::Piece* p = piece_board[j][i];
            if (p == nullptr)
            {
                if (!attack_board[j][i])
                {
                    std::cout << "  ";
                }
                else
                {
                    std::cout << " x";
                }
            }
            else
            {
                std::cout << ' ' << p->defense.size();
            }
        }
        std::cout << " | " << max_y-i << "\n";
    }

    std::cout << "   +-";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << "--";
    }
    std::cout << "+\n";

    std::cout << "    ";
    for (int8_t i = 0; i < max_x; ++i)
    {
        std::cout << " " << char('A'+i);
    }
    std::cout << "\n";
}
