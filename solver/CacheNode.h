#pragma once
#include <cstdint>
#include <vector>
#include <limits>
#include "HistoryElement.h"

namespace Solver
{
    class CacheNode;
}

class Solver::CacheNode
{
public:

    uint16_t steps; // Liczba kroków potrzebna do osiągnięcia danej pozycji
    uint16_t solution; // Rozwiązanie danej pozycji (jeśli nieustalone, równe std::numeric_limits<uint16_t>::max())

    HistoryElement h_elem;

    CacheNode(const uint16_t _steps): steps(_steps), solution(std::numeric_limits<uint16_t>::max()){}
    CacheNode(const uint16_t _steps, const uint16_t _solution): steps(_steps), solution(_solution){}
};
