/**
 * @file solver/ToGenerator.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy ToGenerator.
 */

#pragma once
#include <memory>
#include "../Visitor.h"
#include "../solver/Pawn.h"
#include "../solver/Bishop.h"
#include "../solver/Rook.h"
#include "../solver/Queen.h"
#include "../solver/King.h"
#include "../generator/Pawn.h"
#include "../generator/Bishop.h"
#include "../generator/Rook.h"
#include "../generator/Queen.h"
#include "../generator/King.h"

namespace Solver
{
    class ToGenerator;
}

class Solver::ToGenerator: public Visitor
{
private:

    std::list<std::unique_ptr<Generator::Piece>> generator_pieces;

public:

    virtual void visit(const Solver::Pawn& p);
    virtual void visit(const Solver::Bishop& b);
    virtual void visit(const Solver::Rook& r);
    virtual void visit(const Solver::Queen& q);
    virtual void visit(const Solver::King& k);

    std::list<std::unique_ptr<Generator::Piece>> getPieceList();
};

