/**
 * @file solver/Rook.cpp
 * @author Emil Bałdyga
 */

#include "Rook.h"
#include "Chessboard.h"

Solver::Rook::Rook(const int8_t x, const int8_t y): Solver::Piece(x, y){}

void Solver::Rook::accept(Visitor& v)
{
    v.visit(*this);
}

void Solver::Rook::transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard&, int8_t, int8_t))
{
    int8_t temp;
    temp = x;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_x || (this->*trans_func)(board, temp, y))
        {
            break;
        }
    }
    temp = x;
    for (;;)
    {
        --temp;
        if (temp < 0 || (this->*trans_func)(board, temp, y))
        {
            break;
        }
    }
    temp = y;
    for (;;)
    {
        ++temp;
        if (temp >= board.max_y || (this->*trans_func)(board, x, temp))
        {
            break;
        }
    }
    temp = y;
    for (;;)
    {
        --temp;
        if (temp < 0 || (this->*trans_func)(board, x, temp))
        {
            break;
        }
    }
}
