/**
 * @file solver/ExceptionAttack.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionAttack.h"

Solver::ExceptionAttack::ExceptionAttack(int8_t _x, int8_t _y): x(_x), y(_y){}

const char* Solver::ExceptionAttack::what() const noexcept
{
    return "Starting position under attack!";
}
