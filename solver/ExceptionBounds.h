#pragma once
#include "Exception.h"
/**
 * @file solver/ExceptionBounds.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::ExceptionBounds.
 */

#include <cstdint>

namespace Solver
{
    class ExceptionBounds;
}

class Solver::ExceptionBounds: public Solver::Exception
{
private:

    const int8_t max_x;
    const int8_t max_y;
    const int8_t x;
    const int8_t y;

public:

    ExceptionBounds(int8_t _max_x, int8_t _max_y, int8_t _x, int8_t _y);

    virtual const char* what() const noexcept;
};
