#include "Cache.h"
#include "../PerfectHash.h"

unsigned Solver::Cache::read_count = 0;

Solver::Cache::Cache(const int8_t max_x, const int8_t max_y, const std::vector<Solver::Piece*>& piece_vector,
    const int8_t x, const int8_t y): size(piece_vector.size()), cache((size+1)*size)
{
    pos_map.reserve(size);
    {
        uint8_t i = 0;
        for (std::vector<Solver::Piece*>::const_iterator it = piece_vector.begin(); it != piece_vector.end(); ++it)
        {
            pos_map[std::pair<int8_t, int8_t>((*it)->getX(), (*it)->getY())] = i;
            ++i;
        }
        pos_map[std::pair<int8_t, int8_t>(x, y)] = i;
    }
}

uint128_t Solver::Cache::hashSet(const std::vector<Solver::Piece*>& piece_vector)
{
    constexpr uint128_t one(1);
    uint128_t hash_val(0);
    for (std::vector<Solver::Piece*>::const_iterator it = piece_vector.begin(); it != piece_vector.end(); ++it)
    {
        hash_val |= (one << pos_map[std::pair<int8_t, int8_t>((*it)->getX(), (*it)->getY())]);
    }
    return hash_val;
}

std::pair<bool, Solver::CacheNode*> Solver::Cache::insertFind(const int8_t x, const int8_t y,
        const std::vector<Solver::Piece*>& piece_vector, const CacheNode& c_node)
{
    ++read_count;
    const uint16_t set_size = piece_vector.size();
    return cache[size*pos_map[std::pair<int8_t, int8_t>(x, y)]+(set_size-1)].insertFind(hashSet(piece_vector), c_node);
}
