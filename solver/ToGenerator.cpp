/**
 * @file solver/ToGenerator.cpp
 * @author Emil Bałdyga
 */

#include "ToGenerator.h"

void Solver::ToGenerator::visit(const Solver::Pawn& p)
{
    generator_pieces.push_back(std::make_unique<Generator::Pawn>(p.getX(), p.getY()));
}

void Solver::ToGenerator::visit(const Solver::Bishop& b)
{
    generator_pieces.push_back(std::make_unique<Generator::Bishop>(b.getX(), b.getY()));
}

void Solver::ToGenerator::visit(const Solver::Rook& r)
{
    generator_pieces.push_back(std::make_unique<Generator::Rook>(r.getX(), r.getY()));
}

void Solver::ToGenerator::visit(const Solver::Queen& q)
{
    generator_pieces.push_back(std::make_unique<Generator::Queen>(q.getX(), q.getY()));
}

void Solver::ToGenerator::visit(const Solver::King& k)
{
    generator_pieces.push_back(std::make_unique<Generator::King>(k.getX(), k.getY()));
}

std::list<std::unique_ptr<Generator::Piece>> Solver::ToGenerator::getPieceList()
{
    return std::move(generator_pieces);
}
