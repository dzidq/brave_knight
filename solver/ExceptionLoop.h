/**
 * @file solver/ExceptionLoop.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::ExceptionLoop.
 */

#pragma once
#include "Exception.h"

namespace Solver
{
    class ExceptionLoop;
}

class Solver::ExceptionLoop: public Solver::Exception
{
public:

    ExceptionLoop(){}

    virtual const char* what() const noexcept;
};
