#include "CacheTree.h"
#include <limits>
#include <functional>

Solver::CacheTree::CacheTree()
{
    min_limit = std::numeric_limits<uint16_t>::max();
}

std::pair<bool, Solver::CacheNode*> Solver::CacheTree::insertFind(const uint128_t key, const CacheNode& data)
{
    return c_tree.insertFind(key, data);
}
