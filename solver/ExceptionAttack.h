/**
 * @file solver/ExceptionAttack.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::ExceptionAttack.
 */

#pragma once
#include "Exception.h"
#include <cstdint>

namespace Solver
{
    class ExceptionAttack;
}

class Solver::ExceptionAttack: public Solver::Exception
{
private:

    const int8_t x;
    const int8_t y;

public:

    ExceptionAttack(int8_t x, int8_t y);

    virtual const char* what() const noexcept;
};
