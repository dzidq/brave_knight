/**
 * @file solver/Queen.cpp
 * @author Emil Bałdyga
 */

#include "Queen.h"
#include "Chessboard.h"

Solver::Queen::Queen(const int8_t x, const int8_t y): Solver::Piece(x, y), Solver::Bishop(x, y), Solver::Rook(x, y){}

void Solver::Queen::accept(Visitor& v)
{
    v.visit(*this);
}

void Solver::Queen::transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard&, int8_t, int8_t))
{
    Solver::Bishop::transformBoard(board, trans_func);
    Solver::Rook::transformBoard(board, trans_func);
}
