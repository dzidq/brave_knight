/**
 * @file solver/Solver.h
 * @author Emil Bałdyga
 * @brief Plik zawierający funkcję do rozwiązywania zadań szachowych.
 */

#pragma once
#include <memory>
#include <list>

namespace Game
{
    class Piece;
}

namespace Solver
{
    /**
     * @brief Funkcja interfejsu do klas rozwiązujących zagadki szachowe.
     *
     * Funkcja stanowi zewnętrzny interfejs do łatwego rozwiązywania zadań szachowych. Zajmuje się
     * ona konwersją klas figur (Game::Piece do Solver::Piece) oraz zarządzaniem pamięcią związaną
     * z tą konwersją. Wewnętrznie do rozwiązania jest wykorzystywana klasa Solver::Chessboard.
     * W przypadku, gdy z jakiegoś powodu nie da się rozwiązać zadania, rzucany jest wyjątek
     * Solver::Exception.
     * @param max_x szerokość szachownicy.
     * @param max_y wysokość szachownicy.
     * @param x pozycja startowa skoczka na szachownicy w poziomie.
     * @param y pozycja startowa skoczka na szachownicy w pionie.
     * @param game_pieces lista figur stojących na szachownicy.
     * @return Najmniejsza możliwa liczba kroków, w której da się rozwiązać zagadkę.
     * @see Solver::Chessboard
     * @see Solver::Piece
     * @see Game::Piece
     */
    uint16_t solve(const int8_t max_x, const int8_t max_y, const int8_t x, const int8_t y,
                 const std::list<std::unique_ptr<Game::Piece>>& game_pieces);
}
