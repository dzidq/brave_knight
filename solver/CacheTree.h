#pragma once
#include <utility>
#include <bitset>
#include "CacheNode.h"
#include "../TreeAVL.hpp"
#include "../Types.h"

namespace Solver
{
    class CacheTree;
}

class Solver::CacheTree
{
private:

    TreeAVL<uint128_t, CacheNode> c_tree; // Pair is (set_size, weight)
    uint16_t min_limit;

public:

    CacheTree();

    std::pair<bool, CacheNode*> insertFind(const uint128_t key, const CacheNode& data);
};
