/**
 * @file solver/Bishop.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::Bishop.
 */

#pragma once
#include "Piece.h"

namespace Solver
{
    class Bishop;
}

/**
 * @brief Klasa reprezentująca gońca szachowego.
 *
 * Przeznaczona jest do rozwiązywania zadań szachowych. Zawiera pola oraz metody potrzebne w
 * stosowanych algorytmach. Elementy tej klasy służą do wykonywania obliczeń na szachownicy i
 * są agregowane przez klasę Solver::Chessboard. Klasy te są ze sobą bardzo powiązane
 * (współpracują w celu znalezienia optymalnego rozwiązania), więc Solver::Chessboard jest klasą
 * zaprzyjaźnioną z Solver::Bishop. Obiekty tej klasy mogą zostać wygenerowane z klasy Game::Bishop.
 * @see Solver::Piece
 * @see Solver::Chessboard
 * @see Game::Bishop
 */
class Solver::Bishop: public virtual Solver::Piece
{
protected:

    virtual void transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard& board, int8_t x, int8_t y));

public:

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne figury podanymi argumentami. Domyślnie tworzy pustą listę @p defense
     * i ustawia @p processed na false.
     * @param x współrzędna pozioma.
     * @param y współrzędna pionowa.
     */
    Bishop(const int8_t x, const int8_t y);

    virtual char getSign(){return 'B';}

    virtual void accept(Visitor& v);
};
