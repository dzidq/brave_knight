/**
 * @file solver/ExceptionLoop.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionLoop.h"

const char* Solver::ExceptionLoop::what() const noexcept
{
    return "A loop was detected!";
}
