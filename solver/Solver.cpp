/**
 * @file solver/Solver.cpp
 * @author Emil Bałdyga
 */

#include "Solver.h"
#include "Chessboard.h"
#include "../game/ToSolver.h"
#include "ExceptionBounds.h"
#include "ExceptionLoop.h"
#include "ExceptionAttack.h"
#include "ExceptionUnsolvable.h"
#include "../ExceptionQueueFull.h"
#include "../ExceptionQueueEmpty.h"

#include <iostream> // DEBUG

uint16_t Solver::solve(const int8_t max_x, const int8_t max_y, const int8_t x, const int8_t y,
                       const std::list<std::unique_ptr<Game::Piece>>& game_pieces)
{
    Game::ToSolver ts_visitor;
    for (std::list<std::unique_ptr<Game::Piece>>::const_iterator it = game_pieces.begin();
         it != game_pieces.end(); ++it)
    {
        (*it)->accept(ts_visitor);
    }
    std::list<std::unique_ptr<Solver::Piece>> solver_pieces = ts_visitor.getPieceList();
    Solver::Chessboard board(max_x, max_y, solver_pieces);
    uint16_t solution = 0;

    try
    {
        solution = board.solve(x, y);
    }
    catch (const Solver::ExceptionLoop& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const Solver::ExceptionBounds& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const Solver::ExceptionAttack& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const Solver::ExceptionUnsolvable& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const ExceptionQueueFull& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const ExceptionQueueEmpty& ex)
    {
        std::cout << ex.what() << "\n";
    }
    return solution;
    // Elements of "solver_pieces" are destroyed here
}
