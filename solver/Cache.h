#pragma once
#include <vector>
#include <unordered_map>
#include <utility>
#include "CacheTree.h"
#include "Piece.h"
#include "../HashPair.hpp"

namespace Solver
{
    class Cache;
}

class Solver::Cache
{
private:

    uint8_t size;
    std::vector<Solver::CacheTree> cache; // This is really a 2d array
    std::unordered_map<std::pair<int8_t, int8_t>, uint8_t, HashPair> pos_map;

    uint128_t hashSet(const std::vector<Solver::Piece*>& piece_vector);

public:

    static unsigned read_count;

    Cache(const int8_t max_x, const int8_t max_y, const std::vector<Solver::Piece*>& piece_vector,
        const int8_t x, const int8_t y);

    std::pair<bool, CacheNode*> insertFind(const int8_t x, const int8_t y,
        const std::vector<Solver::Piece*>& piece_vector, const CacheNode& c_node);
};
