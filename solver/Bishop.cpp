/**
 * @file solver/Bishop.cpp
 * @author Emil Bałdyga
 */

#include "Bishop.h"
#include "Chessboard.h"

Solver::Bishop::Bishop(const int8_t x, const int8_t y): Solver::Piece(x, y){}

void Solver::Bishop::accept(Visitor& v)
{
    v.visit(*this);
}

void Solver::Bishop::transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard&, int8_t, int8_t))
{
    int8_t temp_x, temp_y;
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        ++temp_y;
        if (temp_x >= board.max_x || temp_y >= board.max_y || (this->*trans_func)(board, temp_x, temp_y))
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        ++temp_x;
        --temp_y;
        if (temp_x >= board.max_x || temp_y < 0 || (this->*trans_func)(board, temp_x, temp_y))
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        ++temp_y;
        if (temp_x < 0 || temp_y >= board.max_y || (this->*trans_func)(board, temp_x, temp_y))
        {
            break;
        }
    }
    temp_x = x;
    temp_y = y;
    for (;;)
    {
        --temp_x;
        --temp_y;
        if (temp_x < 0 || temp_y < 0 || (this->*trans_func)(board, temp_x, temp_y))
        {
            break;
        }
    }
}
