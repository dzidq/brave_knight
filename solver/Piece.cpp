/**
 * @file solver/Piece.cpp
 * @author Emil Bałdyga
 */

#include "Piece.h"
#include "Chessboard.h"

Solver::Piece::Piece(const int8_t _x, const int8_t _y): ::Piece(_x, _y), on_white((x+y)%2)
{
    init();
}

Solver::Piece::Piece(const Vector v): ::Piece(v), on_white((x+y)%2)
{
    init();
}

void Solver::Piece::init()
{
    atk_field_cnt = 0;
    processed = false;
}

bool Solver::Piece::addDef(Piece* p)
{
    for (std::list<Piece*>::iterator it = defense.begin(); it != defense.end(); ++it)
    {
        if (*it == p) return false;
    }
    defense.push_back(p);
    return true;
}

bool Solver::Piece::removeDef(Piece* p)
{
    for (std::list<Piece*>::iterator it = defense.begin(); it != defense.end(); ++it)
    {
        if (*it == p)
        {
            defense.erase(it);
            return true;
        }
    }
    return false;
}

bool Solver::Piece::attackField(Solver::Chessboard& board, int8_t temp_x, int8_t temp_y)
{
    ++board.attack_board[temp_x][temp_y];
    ++atk_field_cnt;
    Solver::Piece* p = board.piece_board[temp_x][temp_y];
    if (p != nullptr)
    {
        p->addDef(this);
        return true;
    }
    return false;
}

bool Solver::Piece::undoAttackField(Solver::Chessboard& board, int8_t temp_x, int8_t temp_y)
{
    --board.attack_board[temp_x][temp_y];
    Solver::Piece* p = board.piece_board[temp_x][temp_y];
    if (p != nullptr)
    {
        p->removeDef(this);
        return true;
    }
    return false;
}

void Solver::Piece::attack(Solver::Chessboard& board)
{
    transformBoard(board, &Solver::Piece::attackField);
}

void Solver::Piece::undoAttack(Solver::Chessboard& board)
{
    transformBoard(board, &Solver::Piece::undoAttackField);
}
