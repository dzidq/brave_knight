/**
 * @file solver/ExceptionBounds.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionBounds.h"

Solver::ExceptionBounds::ExceptionBounds(int8_t _max_x, int8_t _max_y, int8_t _x, int8_t _y):
max_x(_max_x), max_y(_max_y), x(_x), y(_y){}

const char* Solver::ExceptionBounds::what() const noexcept
{
    return "Solver starting position out of bounds!";
}
