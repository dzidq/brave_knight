/**
 * @file solver/Piece.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::Piece.
 */

#pragma once
#include <list>
#include "../Piece.h"
#include "../Visitor.h"

namespace Solver
{
    class Chessboard;
    class Piece;
}

/**
 * @brief Klasa abstrakcyjna stanowiąca klasę bazową dla figur szachowych.
 *
 * Przeznaczona jest do rozwiązywania zadań szachowych. Zawiera pola oraz metody potrzebne w
 * stosowanych algorytmach. Elementy tej klasy służą do wykonywania obliczeń na szachownicy i
 * są agregowane przez klasę Solver::Chessboard. Klasy te są ze sobą bardzo powiązane
 * (współpracują w celu znalezienia optymalnego rozwiązania), więc Solver::Piece jest klasą
 * zaprzyjaźnioną z Solver::Chessboard. Obiekty tej klasy mogą zostać wygenerowane z klasy Game::Piece.
 * @see Solver::Chessboard
 * @see Game::Piece
 */
class Solver::Piece: public ::Piece
{
friend class Solver::Chessboard;

private:

    void init();

protected:

    const bool on_white; /** Figura stoi na białym polu. */
    std::list<Piece*> defense; /**< Lista wskaźników na inne figury broniące daną figurę. */
    int8_t atk_field_cnt; /** Liczba atakowanych pól */
    bool processed; /**< Flaga przetworzenia wierzchołka używana przez szachownicę w algorytmie DFS. */

    virtual void transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard& board, int8_t x, int8_t y)) = 0;

    /**
     * @brief Metoda wykonująca atak na szachownicy.
     *
     * Inkrementuje pole attack_board szachownicy na odpowiednich współrzędnych. Współrzędne te
     * zależą od typu figury (metoda jest czysto wirtualna, więc nie ma domyślnej implementacji).
     * Dodatkowo wypełniane są listy @p defense dla figur atakowanych (dodawany jest wskaźnik
     * na figurę wywołującą metodę). W ten sposób powstaje (skierowany) graf zależności między
     * poszczególnymi figurami.
     * @param board referencja do szachownicy.
     */
    void attack(Solver::Chessboard& board);

    /**
     * @brief Metoda cofająca atak na szachownicy.
     *
     * Dekrementuje pole attack_board szachownicy na odpowiednich współrzędnych. Współrzędne te
     * zależą od typu figury (metoda jest czysto wirtualna, więc nie ma domyślnej implementacji).
     * Dodatkowo z listy @p defense figur atakowanych usuwane są wskaźniki na figurę wołającą tę
     * metodę. Stan szachownicy powraca do stanu sprzed użycia metody attack() (jeśli została ona
     * wcześniej wykonana).
     * @param board referencja do szachownicy.
     */
    void undoAttack(Solver::Chessboard& board);

public:

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne figury podanymi argumentami. Domyślnie tworzy pustą listę @p defense
     * i ustawia @p processed na false.
     * @param x współrzędna pozioma.
     * @param y współrzędna pionowa.
     */
    Piece(const int8_t x, const int8_t y);

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne figury podanym argumentem. Domyślnie tworzy pustą listę @p defense
     * i ustawia @p processed na false.
     * @param v wektor współrzędnych.
     */
    Piece(const Vector v);

    /// @brief Destruktor.
    virtual ~Piece(){}

    virtual char getSign() = 0; // DEBUG

    /**
     * @brief Metoda dodająca wskaźnik do listy @p defense .
     *
     * Jeśli na liście podany wskaźnik już istnieje, duplikat nie jest wstawiany.
     * @param p wskaźnik na figurę do wstawienia.
     * @return Fałsz, gdy wskaźnik znajduje się już na liście. Prawda w przeciwnym przypadku.
     */
    bool addDef(Solver::Piece* p);

    /**
     * @brief Metoda usuwająca wskaźnik z listy @p defense .
     *
     * Jeśli na liście nie ma podanego wskaźnika, funkcja nic nie robi.
     * @param p wskaźnik na figurę do usunięcia.
     * @return Fałsz, gdy wskaźnik nie został usunięty. Prawda w przeciwnym przypadku.
     */
    bool removeDef(Solver::Piece* p);

    bool attackField(Solver::Chessboard& board, int8_t x, int8_t y);

    bool undoAttackField(Solver::Chessboard& board, int8_t x, int8_t y);

    /**
     * @brief Przetworzenie wierzchołka grafu w algorytmie DFS.
     *
     * Zmienia wartość zmiennej @p processed na true.
     */
    void process(){processed = true;}

    /**
     * @brief Sprawdzenie wartości pola @p processed.
     * @return Prawda jeśli wierzchołek został przetworzony. Fałsz w przeciwnym przypadku.
     */
    bool wasProcessed(){return processed;}

    /**
     * @brief Sprawdzenie czy figura stoi na białym polu.
     * @return Prawda jeśli figura stoi na białym polu.
     */
    bool onWhite(){return on_white;}

    int8_t getAttackFieldCount(){return atk_field_cnt;}

    virtual void accept(Visitor& v) = 0;
};
