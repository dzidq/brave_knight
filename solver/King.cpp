/**
 * @file solver/King.cpp
 * @author Emil Bałdyga
 */

#include "King.h"
#include "Chessboard.h"

Solver::King::King(const int8_t x, const int8_t y): Solver::Piece(x, y){}

void Solver::King::accept(Visitor& v)
{
    v.visit(*this);
}

void Solver::King::transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard&, int8_t, int8_t))
{
    int8_t temp_x;
    int8_t temp_y = y+1;
    if (temp_y < board.max_y)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            (this->*trans_func)(board, temp_x, temp_y);
        }
        (this->*trans_func)(board, x, temp_y);
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            (this->*trans_func)(board, temp_x, temp_y);
        }
    }
    temp_y = y-1;
    if (temp_y >= 0)
    {
        temp_x = x-1;
        if (temp_x >= 0)
        {
            (this->*trans_func)(board, temp_x, temp_y);
        }
        (this->*trans_func)(board, x, temp_y);
        temp_x = x+1;
        if (temp_x < board.max_x)
        {
            (this->*trans_func)(board, temp_x, temp_y);
        }
    }
    temp_x = x+1;
    if (temp_x < board.max_x)
    {
        (this->*trans_func)(board, temp_x, y);
    }
    temp_x = x-1;
    if (temp_x >= 0)
    {
        (this->*trans_func)(board, temp_x, y);
    }
}
