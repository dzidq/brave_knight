/**
 * @file solver/ExceptionUnsolvable.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::ExceptionUnsolvable.
 */

#pragma once
#include "Exception.h"

namespace Solver
{
    class ExceptionUnsolvable;
}

class Solver::ExceptionUnsolvable: public Solver::Exception
{
public:

    ExceptionUnsolvable(){}

    virtual const char* what() const noexcept;
};
