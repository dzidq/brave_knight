/**
 * @file solver/Pawn.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::Pawn.
 */

#pragma once
#include "Piece.h"

namespace Solver
{
    class Pawn;
}

/**
 * @brief Klasa reprezentująca pionka szachowego.
 *
 * Przeznaczona jest do rozwiązywania zadań szachowych. Zawiera pola oraz metody potrzebne w
 * stosowanych algorytmach. Elementy tej klasy służą do wykonywania obliczeń na szachownicy i
 * są agregowane przez klasę Solver::Chessboard. Klasy te są ze sobą bardzo powiązane
 * (współpracują w celu znalezienia optymalnego rozwiązania), więc Solver::Chessboard jest klasą
 * zaprzyjaźnioną z Solver::Pawn. Obiekty tej klasy mogą zostać wygenerowane z klasy Game::Pawn.
 * @see Solver::Piece
 * @see Solver::Chessboard
 * @see Game::Pawn
 */
class Solver::Pawn: public Solver::Piece
{
protected:

    virtual void transformBoard(Solver::Chessboard& board, bool (Solver::Piece::* trans_func)(Solver::Chessboard& board, int8_t x, int8_t y));

public:

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne figury podanymi argumentami. Domyślnie tworzy pustą listę @p defense
     * i ustawia @p processed na false.
     * @param x współrzędna pozioma.
     * @param y współrzędna pionowa.
     */
    Pawn(const int8_t x, const int8_t y);

    virtual char getSign(){return /*L'\u265F'*/'P';}

    virtual void accept(Visitor& v);
};
