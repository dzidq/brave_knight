#pragma once
#include "../Vector.h"

namespace Solver
{
    class HistoryElement;
}

class Solver::HistoryElement
{
public:

    Vector position;
    HistoryElement* next;

    HistoryElement(): next(nullptr){}
};
