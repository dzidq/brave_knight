/**
 * @file solver/ExceptionUnsolvable.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionUnsolvable.h"

const char* Solver::ExceptionUnsolvable::what() const noexcept
{
    return "No solution was found!";
}

