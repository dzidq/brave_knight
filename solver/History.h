#pragma once
#include <vector>
#include "HistoryElement.h"

namespace Solver
{
    class History;
}

class Solver::History
{
private:

    HistoryElement* coupling;

public:

    History();

    void setCoupling(HistoryElement* next);
    HistoryElement* getCoupling();

    void print();
};
