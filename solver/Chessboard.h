/**
 * @file solver/Chessboard.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::Chessboard.
 */

#pragma once
#include <memory>
#include <list>
#include <vector>
#include "../Vector.h"
#include "../FixedQueue.hpp"
#include "Cache.h"
#include "History.h"

namespace Solver
{
    class Chessboard;
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
}

/**
 * @brief Klasa przechowująca figury szachowe.
 *
 * Jest odpowiedzialna za rozwiązywanie zadań szachowych z użyciem własnych metod oraz metod klasy
 * Solver::Piece. Z tego powodu jest ona zaprzyjaźniona z klasami pochodnymi klasy Solver::Piece.
 * Klasa sama nie wytwarza obiektów typu Solver::Piece, operuje jedynie wskaźnikami nie (obiekty
 * są modyfikowane, ale ich pamięć nie jest zwalniana przez szachownicę). Algorytm rozwiązania
 * zadania polega na rekurencyjnym tworzeniu szachownicy o coraz mniejszej liczbie figur i
 * symulowaniu na niej ruchów skoczka we wszystkie strony. Dodatkowo zapamiętywane są zestawy
 * pozycji figur z informacją o liczbie kroków, w jakiej można daną pozycję osiągnąć. Aby rozwiązać
 * zagadkę z użyciem solvera, należy wykorzystać funkcję Solver::solve() (tworzy ona istancję klasy
 * Solver::Chessboard i zajmuje się konwersją klasy Game::Piece na Solver::Piece).
 * @see Solver::Piece
 * @see Solver::solve()
 */
class Solver::Chessboard
{
friend class Solver::Piece;
friend class Solver::Pawn;
friend class Solver::Bishop;
friend class Solver::Rook;
friend class Solver::Queen;
friend class Solver::King;

typedef std::vector<std::vector<Solver::Piece*>> PiecePtr2d;
typedef std::vector<std::vector<int8_t>> Int8_2d;

private:

    const int8_t max_x; /**< Szerokość szachownicy */
    const int8_t max_y; /**< Wysokość szachownicy */
    const int8_t max_field; /**< Liczba pól szachownicy */
    int8_t color_shift; /** Przewaga liczebna figur stojących na polach czarnych nad figurami stojącymi na polach białych */
    bool on_white; /** Czy skoczek znajduje się w danej pozycji na białym polu */

    /// Tablica wskaźników na figury szachowe.
    /** Ma wymiary @p max_x oraz @p max_y . Jeżeli na danym polu nie ma żadnej figury, przyjmuje
     *  ono wartość nullptr. */
    PiecePtr2d piece_board;

    /// Tablica liczby figur broniących dane pole.
    /** Ma wymiary @p max_x oraz @p max_y . Wartość poszczególnych pól odpowiada liczbie figur
     *  atakujących dane pole. Żadne z pól nie powinno przyjmować wartości ujemnych. */
    Int8_2d attack_board;

    /// Wektor wskaźników na figury znajdujące się na szachownicy.
    std::vector<Solver::Piece*> piece_vector;

    /**
     * @brief Metoda rekurencyjna tworząca szachownice i obliczająca optymalne rozwiązanie.
     *
     * Metoda rozwiązuje problem szachowy zmodyfikowanym algorytmem brute-force. Jest wywoływana
     * w funkcji solve() po sprawdzeniu poprawności parametrów wejściowych. Zamiast zwracania
     * wartości, zmienia wartość zmiennej @p limit podanej przez referencję. Jeśli zadanie nie ma
     * rozwiązania, wartość zmiennej limit jest ustawiana na maksymalną możliwą. Jeśli rozwiązanie
     * istnieje, do zmiennej @p limit powinna zostać przypisana wartość określająca minimalną
     * liczbę kroków, w której da się rozwiązać zagadkę.
     * @param x pozycja startowa skoczka w poziomie.
     * @param y pozycja startowa skoczka w pionie.
     * @param weight liczba kroków wykonana do tej pory.
     * @param limit najmniejsza liczba kroków, w jakiej udało się rozwiązać zagadkę.
     * Przyjmuje wartość maksymalną jeśli rozwiązanie nie istnieje.
     * @param positions obiekt określający znalezione dotychczas pozycje na szachownicy.
     * @see Solver::SizeList
     * @see Solver::WeightSet
     */
    //void split(const int8_t x, const int8_t y, const uint16_t weight, uint16_t& limit, Cache& positions);

    uint16_t split(const int8_t x, const int8_t y, const uint16_t weight, uint16_t& limit,
        Cache& positions, History& history);

    /**
     * @brief Metoda znajdująca korzenie w grafie.
     *
     * Korzenie są zdefiniowane jako wierzchołki, które nie są osiągalne z żadnego innego
     * wierzchołka. Wierzchołkami w grafie są poszczególne figury szachowe, a krawędziami
     * zależności ich wzajemnego bronienia się (jeśli figura A broni figurę B, to krawędź jest
     * skierowana z A do B).
     * @return Lista wskaźników na figury, które nie są bronione.
     */
    std::list<Solver::Piece*> getRoots();

    /**
     * @brief Wypełnienie pól szachownicy liczba kroków, w jakiej można się na dane pole dostać.
     *
     * Funkcja wypełnia pola tablicy @p weights liczbami całkowitymi w taki sposób, że każda liczba
     * reprezentuje liczbę kroków skoczka, w której można dostać się z pozycji ( @p x , @p y ) na
     * dane pole zgodnie z zasadami gry (bez wchodzenia pod bicie). Jeśli nie da się dostać na
     * któreś z pól lub trzeba wcześniej zbić figurę, żeby się na takie pole dostać, w tablicy
     * umieszczana jest wartość @p max_field (jeśli ruch skoczka zbija figurę stojącą na danym
     * polu, to wartość tego pola jest mniejsza niż @p max_field ). Funkcja zakłada, że tablica @p
     * weights ma wymiary @p max_x na @p max_y oraz że pozycja ( @p x , @p y ) nie wykracza poza
     * rozmiar tablicy.
     * @param x pozycja startowa skoczka w poziomie.
     * @param y pozycja startowa skoczka w pionie.
     * @param weights dwuwymiarowa tablica int8_t o rozmiarze @p max_x na @p max_y .
     * @see Solver::Chessboard::markWeights()
     * @see Solver::Chessboard::setWeight()
     */
    void calculateWeights(const int8_t x, const int8_t y, Int8_2d& weights);

    /**
     * @brief Oznaczenie pól szachownicy osiągalnych z danej pozycji.
     *
     * Metoda używana wewnętrznie przez Solver::Chessboard::calculateWeights(). Zaznacza osiągalne
     * pola tablicy @p weights z pozycji ( @p x , @p y ) liczbą kroków w jakich można się do nich
     * dostać (wykonuje pojedynczy ruch skokcza w każdym kierunku). Jeśli do któregoś z pól można
     * dostać się w mniejszej liczbie kroków, pole te pozostaje niezmienione (wybierana jest zawsze
     * mniejsza wartość). Wewnętrznie używa metody Solver::Chessboard::setWeight().
     * @param x pozycja skoczka w poziomie.
     * @param y pozycja skoczka w pionie.
     * @param weights dwuwymiarowa tablica int8_t o rozmiarze @p max_x na @p max_y .
     * @param field_queue kolejka pozycji do oznaczenia.
     * @see Solver::Chessboard::calculateWeights()
     * @see Solver::Chessboard::setWeight()
     */
    void markWeights(const int8_t x, const int8_t y, Int8_2d& weights, FixedQueue<Vector>& field_queue);

    /**
     * @brief Oznaczenie liczbą kroków pojedynczego pola szachownicy.
     *
     * Funkcja wywoływana dla każdego osiągnalnego pola przez Solver::Chessboard::markWeights().
     * W przypadku, gdy dane pole jest już oznaczone, konflikt jest rozstrzygany na korzyść
     * mniejszej wartości. Jeśli wartość pola została zmieniona i na danym polu nie ma żadnej
     * figury, to dane pole jest wstawiane do kolejki i będzie analizowane po powrocie do
     * funkcji Solver::Chessboard::calculateWeights().
     * @param x pozycja skoczka w poziomie.
     * @param y pozycja skoczka w pionie.
     * @param weight referencja na wagę pola ( @p x , @p y ).
     * @param value liczba kroków wykonana do tej pory.
     * @param field_queue kolejka osiągalnych pozycji do rozpatrzenia.
     * @see Solver::Chessboard::calculateWeights()
     * @see Solver::Chessboard::markWeights()
     */
    void setWeight(const int8_t x, const int8_t y, int8_t& weight, const int8_t value, FixedQueue<Vector>& field_queue);

    /**
     * @brief Sortowanie listy figur na podstawie tablicy wag pól.
     *
     * Na początek listy są przesuwane elementy stojące na pozycjach, dla których @p weights
     * przyjmuje najmniejszą wartość. Wykorzystywany algorytm sortowania nie jest stabilny. Aby
     * algorytm działał prawidłowo, wcześniej musi zostać wykonana metoda obliczająca liczbę
     * kroków, w jakiem można dostać się do każdego pola (Solver::Chessboard::calculateWeights()).
     * @param pieces lista figur do posortowania.
     * @param weights tablica wag poszczególnych pól szachownicy.
     * @see Solver::Chessboard::calculateWeights()
     */
    //void sortByWeight(std::list<Solver::Piece*>& pieces, const Int8_2d& weights);

    void sortCutByWeight(std::list<Solver::Piece*>& pieces, const Int8_2d& weights);

    /**
     * @brief Inicjalizacja grafu zależności pomiędzy figurami.
     *
     * Metoda wykonuje symulację ataku wszystkich figur na szachownicy i uzupełnia listy figur
     * obrony poszczególnych figur. Dodatkowo każdemu polu zostaje przypisana liczba figur
     * broniących to pole (tablica @p attack_board ).
     * @see Solver::Piece::attack()
     */
    void initializeGraph();

    /**
     * @brief Metoda sprawdzająca istnieje cykli w grafie skierowanym.
     *
     * Metoda wykonuje algorytm DFS w celu wykrycia cykli w grafie. Zakłada, że graf został w pełni
     * zainicjalizowany za pomocą metody Solver::Chessboard::initializeGraph(). Wewnętrznie używa
     * pomocniczej funkcji Solver::Chessboard::hasLoopsDFS().
     * @return Prawda, jeśli istnieją cykle w grafie. Fałsz w przeciwnym przypadku.
     * @see Solver::Chessboard::hasLoopsDFS()
     * @see Solver::Chessboard::initializeGraph()
     */
    bool hasLoops();

    /**
     * @brief Funkcja pomocnicza do algorytmu znajdowania cykli w grafie.
     *
     * Funkcja rekurencyjna wykonywana dla każdego wierzchołka grafu (każdej figury). Używana jest
     * wewnątrz metody Solver::Chessboard::hasLoops().
     * @param reached lista wierzchołków odwiedzonych w danej ścieżce.
     * @param p wierzchołek obecnie odwiedzany.
     * @return Prawda, jeśli wierzchołek należy do cyklu. Fałsz w przeciwnym przypadku.
     * @see Solver::Chessboard::hasLoops()
     */
    bool hasLoopsDFS(std::list<Solver::Piece*>& reached, Solver::Piece* p);

    bool allReachable(const int8_t x, const int8_t y);

    void randPermute();

    // Calculate intermediate steps for the solution
    std::vector<Vector> intermediateSteps(const int8_t x, const int8_t y, const HistoryElement* step_list, const uint16_t limit);

    Chessboard(const Chessboard& board, Solver::Piece* omit); // Shallow copy constructor

public:

    /**
     * @brief Konstruktor.
     *
     * Rezerwuje pamięć na szachownicę o rozmiarach @p max_x na @p max_y i wypełnia struktury
     * danych informacjami o figurach stojących na szachownicy.
     * @param max_x szerokość szachownicy.
     * @param max_y wysokość szachownicy.
     * @param pieces lista figur stojących na szachownicy.
     */
    Chessboard(const int8_t max_x, const int8_t max_y, const std::list<std::unique_ptr<Solver::Piece>>& pieces);

    /**
     * @brief Rozwiązywanie zadania szachowego.
     *
     * Metoda znajduje najmniejszą możliwą liczbę kroków, w której można zbić skoczkiem wszystkie
     * figury przeciwnika na szachownicy z pozycji ( @p x , @p y ) bez wchodzenia pod bicie.
     * Funkcja sprawdza poprawność danych wejściowych i rozwiązywalność zadania. Jeśli dane
     * wejściowe nie są poprawne (np. nie istnieje rozwiązanie), rzucany jest wyjątek
     * Solver::Exception.
     * @return Najmniejsza możliwa liczba kroków, w której da się rozwiązać zagadkę.
     */
    uint16_t solve(const int8_t x, const int8_t y);

    // Temporary console draw functions (debug purpose)
    void drawMergedA(); // Draw pieces on top of attacked positions
    void drawMergedB(); // Draw pieces on top of attacked positions (pieces are represented by the numbers of other pieces defending them)
};
