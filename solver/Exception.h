/**
 * @file solver/Exception.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Solver::Exception.
 */

#pragma once
#include <exception>

namespace Solver
{
    class Exception;
}

class Solver::Exception: public std::exception
{

};
