#include "History.h"

#include <iostream> // DEBUG

Solver::History::History(): coupling(nullptr){}

void Solver::History::setCoupling(HistoryElement* next)
{
    coupling = next;
}

Solver::HistoryElement* Solver::History::getCoupling()
{
    return coupling;
}

void Solver::History::print()
{
    HistoryElement* curr_elem = coupling;
    while (curr_elem)
    {
        std::cout << "(" << (int)curr_elem->position.x << "; " << (int)curr_elem->position.y << ")\n";
        curr_elem = curr_elem->next;
    }
    std::cout << std::endl;
}
