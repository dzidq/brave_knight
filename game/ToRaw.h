/**
 * @file game/ToRaw.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::ToRaw.
 */

#pragma once
#include <memory>
#include <list>
#include "../Visitor.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"
#include "../raw/Piece.h"

namespace Game
{
    class ToRaw;
}

class Game::ToRaw: public Visitor
{
private:

    std::list<Raw::Piece> raw_pieces;

public:

    virtual void visit(const Game::Pawn& p);
    virtual void visit(const Game::Bishop& b);
    virtual void visit(const Game::Rook& r);
    virtual void visit(const Game::Queen& q);
    virtual void visit(const Game::King& k);

    std::list<Raw::Piece> getPieceList();
};

