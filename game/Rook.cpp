/**
 * @file game/Rook.cpp
 * @author Emil Bałdyga
 */

#include "Rook.h"
#include "../solver/Rook.h"

Game::Rook::Rook(const int8_t x, const int8_t y): Game::Piece(x, y){}

void Game::Rook::accept(Visitor& v)
{
    v.visit(*this);
}
