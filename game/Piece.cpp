/**
 * @file game/Piece.cpp
 * @author Emil Bałdyga
 */

#include "Piece.h"

Game::Piece::Piece(const int8_t _x, const int8_t _y): ::Piece(_x, _y){}

Game::Piece::Piece(const Vector v): ::Piece(v){}
