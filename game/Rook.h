/**
 * @file game/Rook.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::Rook.
 */

#pragma once
#include "Piece.h"

namespace Game
{
    class Rook;
}

class Game::Rook: public virtual Game::Piece
{
public:

    Rook(const int8_t x, const int8_t y);

    virtual void accept(Visitor& v);
};
