/**
 * @file game/Piece.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::Piece.
 */

#pragma once
#include "../Piece.h"
#include "../VisitElement.h"

namespace Solver
{
    class Piece;
}

namespace Game
{
    class Chessboard;
    class Piece;
}

class Game::Piece: public ::Piece, public VisitElement
{
friend class Game::Chessboard;

public:

    Piece(const int8_t x, const int8_t y);
    Piece(const Vector v);
    virtual ~Piece(){}

    virtual void accept(Visitor& v) = 0;
};
