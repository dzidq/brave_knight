/**
 * @file game/Pawn.cpp
 * @author Emil Bałdyga
 */

#include "Pawn.h"
#include "../solver/Pawn.h"

Game::Pawn::Pawn(const int8_t x, const int8_t y): Game::Piece(x, y){}

void Game::Pawn::accept(Visitor& v)
{
    v.visit(*this);
}
