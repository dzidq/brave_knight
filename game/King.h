/**
 * @file game/King.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::King.
 */

#pragma once
#include "Piece.h"

namespace Game
{
    class King;
}

class Game::King: public Game::Piece
{
public:

    King(const int8_t x, const int8_t y);

    virtual void accept(Visitor& v);
};
