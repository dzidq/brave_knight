/**
 * @file game/Bishop.cpp
 * @author Emil Bałdyga
 */

#include "Bishop.h"
#include "../solver/Bishop.h"

Game::Bishop::Bishop(const int8_t x, const int8_t y): Game::Piece(x, y){}

void Game::Bishop::accept(Visitor& v)
{
    v.visit(*this);
}
