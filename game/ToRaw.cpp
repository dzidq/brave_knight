/**
 * @file game/ToRaw.cpp
 * @author Emil Bałdyga
 */

#include "ToRaw.h"

void Game::ToRaw::visit(const Game::Pawn& p)
{
    raw_pieces.push_back(Raw::Piece(Ptype::PAWN, p.getX(), p.getY()));
}

void Game::ToRaw::visit(const Game::Bishop& b)
{
    raw_pieces.push_back(Raw::Piece(Ptype::BISHOP, b.getX(), b.getY()));
}

void Game::ToRaw::visit(const Game::Rook& r)
{
    raw_pieces.push_back(Raw::Piece(Ptype::ROOK, r.getX(), r.getY()));
}

void Game::ToRaw::visit(const Game::Queen& q)
{

    raw_pieces.push_back(Raw::Piece(Ptype::QUEEN, q.getX(), q.getY()));
}

void Game::ToRaw::visit(const Game::King& k)
{
    raw_pieces.push_back(Raw::Piece(Ptype::KING, k.getX(), k.getY()));
}

std::list<Raw::Piece> Game::ToRaw::getPieceList()
{
    return std::move(raw_pieces);
}

