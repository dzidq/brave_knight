/**
 * @file game/Queen.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::Queen.
 */

#pragma once
#include "Bishop.h"
#include "Rook.h"

namespace Game
{
    class Queen;
}

class Game::Queen: public Game::Bishop, public Game::Rook
{
public:

    Queen(const int8_t x, const int8_t y);

    virtual void accept(Visitor& v);
};
