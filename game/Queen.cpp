/**
 * @file game/Queen.cpp
 * @author Emil Bałdyga
 */

#include "Queen.h"
#include "../solver/Queen.h"

Game::Queen::Queen(const int8_t x, const int8_t y): Game::Piece(x, y), Game::Bishop(x, y), Game::Rook(x, y){}

void Game::Queen::accept(Visitor& v)
{
    v.visit(*this);
}
