/**
 * @file game/ToSolver.cpp
 * @author Emil Bałdyga
 */

#include "ToSolver.h"

void Game::ToSolver::visit(const Game::Pawn& p)
{
    solver_pieces.push_back(std::make_unique<Solver::Pawn>(p.getX(), p.getY()));
}

void Game::ToSolver::visit(const Game::Bishop& b)
{
    solver_pieces.push_back(std::make_unique<Solver::Bishop>(b.getX(), b.getY()));
}

void Game::ToSolver::visit(const Game::Rook& r)
{
    solver_pieces.push_back(std::make_unique<Solver::Rook>(r.getX(), r.getY()));
}

void Game::ToSolver::visit(const Game::Queen& q)
{
    solver_pieces.push_back(std::make_unique<Solver::Queen>(q.getX(), q.getY()));
}

void Game::ToSolver::visit(const Game::King& k)
{
    solver_pieces.push_back(std::make_unique<Solver::King>(k.getX(), k.getY()));
}

std::list<std::unique_ptr<Solver::Piece>> Game::ToSolver::getPieceList()
{
    return std::move(solver_pieces);
}
