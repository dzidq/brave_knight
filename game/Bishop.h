/**
 * @file game/Bishop.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::Bishop.
 */

#pragma once
#include "Piece.h"

namespace Game
{
    class Bishop;
}

class Game::Bishop: public virtual Game::Piece
{
public:

    Bishop(const int8_t x, const int8_t y);

    virtual void accept(Visitor& v);
};
