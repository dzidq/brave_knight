/**
 * @file game/Pawn.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::Pawn.
 */

#pragma once
#include "Piece.h"

namespace Game
{
    class Pawn;
}

class Game::Pawn: public Game::Piece
{
public:

    Pawn(const int8_t x, const int8_t y);

    virtual void accept(Visitor& v);
};
