/**
 * @file game/ToSolver.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Game::ToSolver.
 */

#pragma once
#include <memory>
#include "../Visitor.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"
#include "King.h"
#include "../solver/Pawn.h"
#include "../solver/Bishop.h"
#include "../solver/Rook.h"
#include "../solver/Queen.h"
#include "../solver/King.h"

namespace Game
{
    class ToSolver;
}

class Game::ToSolver: public Visitor
{
private:

    std::list<std::unique_ptr<Solver::Piece>> solver_pieces;

public:

    virtual void visit(const Game::Pawn& p);
    virtual void visit(const Game::Bishop& b);
    virtual void visit(const Game::Rook& r);
    virtual void visit(const Game::Queen& q);
    virtual void visit(const Game::King& k);

    std::list<std::unique_ptr<Solver::Piece>> getPieceList();
};
