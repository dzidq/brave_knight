/**
 * @file game/King.cpp
 * @author Emil Bałdyga
 */

#include "King.h"
#include "../solver/King.h"

Game::King::King(const int8_t x, const int8_t y): Game::Piece(x, y){}

void Game::King::accept(Visitor& v)
{
    v.visit(*this);
}
