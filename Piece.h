/**
 * @file Piece.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Piece.
 */

#pragma once
#include "Vector.h"

/**
 * @brief Klasa bazowa dla figur szachowych.
 *
 * Zawiera pola będące współrzędnymi figury na szachownicy (oś X jest skierowana w prawo, a oś Y
 * jest skierowana w dół). Implementuje proste metody dostępu, pozwalające na odczyt oraz
 * modyfikację tych współrzędnych.
 */
class Piece
{
protected:

    int8_t x; /**< Pozycja figury w poziomie. */
    int8_t y; /**< Pozycja figury w pionie. */

public:

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne figury podanymi argumentami.
     * @param _x współrzędna pozioma.
     * @param _y współrzędna pionowa.
     */
    Piece(const int8_t _x, const int8_t _y): x(_x), y(_y){}

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne figury argumentami podanymi z wektora.
     * @param v wektor współrzędnych.
     */
    Piece(const Vector v): x(v.x), y(v.y){}

    /**
     * @brief Ustawienie pozycji na podstawie wektora.
     * @param v wektor współrzędnych.
     */
    void setPosition(const Vector v){x = v.x; y = v.y;}

    /**
     * @brief Pobranie pozycji figury.
     * @return Wektor współrzędnych zawierający pozycję figury.
     */
    const Vector getPosition() const {return Vector(x, y);}

    /**
     * @brief Pobranie współrzędnej poziomej figury.
     * @return Współrzędna pozioma figury.
     */
    const int8_t getX() const {return x;}

    /**
     * @brief Pobranie współrzędnej pionowej figury.
     * @return Współrzędna pionowa figury.
     */
    const int8_t getY() const {return y;}
};
