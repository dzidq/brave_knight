/**
 * @file Vector.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Vector.
 */

#pragma once
#include <cstdint>

/**
 * @brief Kontener przechowujący dwuwymiarowy wektor 8-bitowy.
 *
 * Wektor służy do przechowywania pozycji figur na szachownicy. Ze względu na to, że zakładany
 * rozmiar szachownicy jest nieduży, pozycja jest przechowywana w zmiennych 8-bitowych ze znakiem.
 * Klasa ma zdefiniowane operatory porównania, co pozwala uporządkować pozycje (daje możliwość
 * posortowania kolekcji wektorów). Wartości ujemne są zarezerwowane dla sytuacji błędnych (np.
 * przy próbie wstawienia nowej figury na zajęte pole). Zakłada się, że oś pozioma jest skierowana
 * w prawo, a oś pionowa w dół.
 */
class Vector
{
public:

    int8_t x; /**< Pozycja w poziomie. */
    int8_t y; /**< Pozycja w pionie. */

    /**
     * @brief Konstruktor domyślny.
     *
     * Pozostawia pola niezainicjalizowane w celu zwiększenia wydajności.
     */
    Vector();

    /**
     * @brief Konstruktor.
     *
     * Wypełnia współrzędne wektora podanymi argumentami.
     * @param x współrzędna pozioma.
     * @param y współrzędna pionowa.
     */
    Vector(const int8_t x, const int8_t y);

    /**
     * @brief Operator równości.
     *
     * Sprawdza czy wartości odpowiednich współrzędnych @p v1 i @p v2 są jednakowe.
     * @param v1 pierwszy wektor do porównania.
     * @param v2 drugi wektor do porównania.
     */
    friend bool operator== (const Vector &v1, const Vector &v2);

    /**
     * @brief Operator nierówności.
     *
     * Sprawdza czy przynajmniej jedna współrzędna @p v1 i @p v2 jest różna.
     * @param v1 pierwszy wektor do porównania.
     * @param v2 drugi wektor do porównania.
     */
    friend bool operator!= (const Vector &v1, const Vector &v2);

    /**
     * @brief Operator większości.
     *
     * Stosuje odwrócony porządek leksykograficzny {y, x}. Sprawdza czy współrzędna pionowa @p v1
     * jest większa niż @p v2. Jeśli są równe, porównywana jest współrzędna pozioma. Np. wektor
     * (3, 5) jest większy niż wektor (5, 3).
     *
     * @param v1 pierwszy wektor do porównania.
     * @param v2 drugi wektor do porównania.
     */
    friend bool operator> (const Vector &v1, const Vector &v2);

    /**
     * @brief Operator mniejszości.
     *
     * Stosuje odwrócony porządek leksykograficzny {y, x}. Sprawdza czy współrzędna pionowa @p v1
     * jest mniejsza niż @p v2. Jeśli są równe, porównywana jest współrzędna pozioma. Np. wektor
     * (5, 3) jest mniejszy niż wektor (3, 5).
     *
     * @param v1 pierwszy wektor do porównania.
     * @param v2 drugi wektor do porównania.
     */
    friend bool operator< (const Vector &v1, const Vector &v2);
};
