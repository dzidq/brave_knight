/**
 * @file VisitElement.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy VisitElement.h
 */

#pragma once
#include "Visitor.h"

/**
 * @brief Klasa implementująca element ze wzorca wizytatora.
 *
 * Klasa pozwala akceptować dowolny wizytator i wykonać dowolną operację na wizytatorze.
 */
class VisitElement
{
public:

    virtual void accept(Visitor& v) = 0;
};
