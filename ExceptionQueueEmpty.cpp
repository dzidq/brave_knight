/**
 * @file ExceptionQueueEmpty.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionQueueEmpty.h"

const char* ExceptionQueueEmpty::what() const noexcept
{
    return "Queue is empty!";
}

