/**
 * @file Vector.cpp
 * @author Emil Bałdyga
 */

#include "Vector.h"

Vector::Vector()
{
    // Uninitialized for performance gain!
}

Vector::Vector(const int8_t _x, const int8_t _y): x(_x), y(_y){}

bool operator== (const Vector &v1, const Vector &v2)
{
    return v1.x == v2.x && v1.y == v2.y;
}

bool operator!= (const Vector &v1, const Vector &v2)
{
    return v1.x != v2.x || v1.y != v2.y;
}

bool operator> (const Vector &v1, const Vector &v2)
{
    return v1.y > v2.y || (v1.y == v2.y && v1.x > v2.x);
}

bool operator< (const Vector &v1, const Vector &v2)
{
    return v1.y < v2.y || (v1.y == v2.y && v1.x < v2.x);
}
