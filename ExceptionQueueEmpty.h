/**
 * @file ExceptionQueueEmpty.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy ExceptionQueueEmpty.
 */

#pragma once
#include "Exception.h"

class ExceptionQueueEmpty: public Exception
{
public:

    virtual const char* what() const noexcept;
};

