/**
 * @file main.cpp
 * @author Emil Bałdyga
 */

#include "solver/Solver.h"
#include "generator/Generator.h"
#include "editor/CheckSolvable.h"

#include "game/Pawn.h"
#include "game/Bishop.h"
#include "game/Rook.h"
#include "game/Queen.h"
#include "game/King.h"

#include "RandGen.hpp"

#include <iostream>
#include <chrono>

#include "NodeAVL.hpp"
#include "solver/CacheNode.h"
#include "Types.h"

#include "raw/ReadTest.h"
#include "TestGenerator.h"

int main()
{
    Generator::Parameters params(Generator::Parameters::PDF_MODE::EQUAL);
    params.setPieceNumber(100);
    params.setBoardSize(10, 10);
    params.setKnightPosition(0, 0);

    params.bake();

    const unsigned ITERATIONS = 1;
    unsigned gen_time = 0;
    unsigned sol_time = 0;

    for (unsigned i = 0; i < ITERATIONS; ++i)
    {
    std::list<std::unique_ptr<Game::Piece>> piece_list;
    Vector board_size = params.getBoardSize();
    Vector start_pos = params.getKnightPosition();

    auto gen_start_time = std::chrono::system_clock::now();
    piece_list = Generator::generate(params);
    auto gen_end_time = std::chrono::system_clock::now();

    auto sol_start_time = std::chrono::system_clock::now();
    Solver::solve(board_size.x, board_size.y, start_pos.x, start_pos.y, std::move(piece_list));
    auto sol_end_time = std::chrono::system_clock::now();

    gen_time += std::chrono::duration_cast<std::chrono::milliseconds>(gen_end_time-gen_start_time).count();
    sol_time += std::chrono::duration_cast<std::chrono::milliseconds>(sol_end_time-sol_start_time).count();
    }

    std::cout << "Generation time: " << gen_time << " ms\n";
    std::cout << "Solving time: " << sol_time << " ms\n";
    std::cout << "AVL tree rotation count: " << NodeAVL<uint128_t, Solver::CacheNode>::rotation_count << std::endl;
    std::cout << "AVL tree insertion count: " << NodeAVL<uint128_t, Solver::CacheNode>::insertion_count << std::endl;
    std::cout << "AVL rotations to insertions ratio: " <<
        static_cast<double>(NodeAVL<uint128_t, Solver::CacheNode>::rotation_count)/
            NodeAVL<uint128_t, Solver::CacheNode>::insertion_count << std::endl;

    return 0;
}
