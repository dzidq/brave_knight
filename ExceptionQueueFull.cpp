/**
 * @file ExceptionQueueFull.cpp
 * @author Emil Bałdyga
 */

#include "ExceptionQueueFull.h"

const char* ExceptionQueueFull::what() const noexcept
{
    return "Queue is full!";
}
