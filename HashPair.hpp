#pragma once
#include <utility>
#include <functional>

template <typename T>
static std::size_t hash_combine(std::size_t seed, const T& val)
{
    seed ^= std::hash<T>{}(val) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
}

class HashPair
{
public:

    template <typename T1, typename T2>
    size_t operator()(const std::pair<T1, T2>& p) const
    {
        size_t seed = 0;
        seed = hash_combine(seed, p.first);
        return hash_combine(seed, p.second);
    }
};
