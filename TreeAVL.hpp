#pragma once
#include "NodeAVL.hpp"
#include <utility>
#include <functional>
#include <vector>

template <typename TK, typename TD>
class TreeAVL
{
private:

    /// Wskaźnik na korzeń drzewa AVL.
    std::unique_ptr<NodeAVL<TK, TD>> root;

    /// Liczba elementów w drzewie (łącznie z korzeniem).
    unsigned size;

public:

    TreeAVL(): size(0){}

    TreeAVL(const TreeAVL& tree)
    {
        if (tree.root)
            root = std::make_unique<NodeAVL<TK, TD>>(*tree.root, &root);
        size = tree.size;
    }

    TreeAVL& operator=(const TreeAVL& tree)
    {
        if (this == &tree)
           return *this;
        if (tree.root)
            root = std::make_unique<NodeAVL<TK, TD>>(*tree.root, &root);
        size = tree.size;
        return *this;
    }

    unsigned getSize(){return size;}

    /**
     * @brief Wstaw element do drzewa lub zwróć istniejący element o podanym kluczu.
     *
     * Metoda szuka w drzewie elementu o podanym kluczu. W przypadku znalezienia takiego elementu
     * zwraca wskaźnik do niego i sygnalizuje to flagą ustawioną na "true". W przeciwnym przypadku
     * wstawia nowy element podany jako drugi argument funkcji i zwraca wskaźnik do niego i
     * sygnalizuje to flagą ustawioną na "false".
     * Z założenia @p key powinien być kluczem elementu @p data .
     * @param key klucz wstawianego a jednocześnie szukanego elementu.
     * @param data element do wstawienia w drzewie.
     * @return Para prawda i wskaźnik do znalezionego obiektu o danym kluczu, jeśli istnieje.
     * W przeciwnym przypadku para fałsz i wskaźnik do nowo wstawionego elementu.
     */
    std::pair<bool, TD*> insertFind(const TK key, const TD& data)
    {
        if (root)
        {
            const std::tuple<bool, TD*, bool> ret_tup = root->insertFind(key, data);
            TD* ret_ptr = std::get<1>(ret_tup);
            if (!std::get<0>(ret_tup))
                ++size;
            return std::pair<bool, TD*>(std::get<0>(ret_tup), ret_ptr);
        }
        root = std::make_unique<NodeAVL<TK, TD>>(&root, key, data);
        ++size;
        return std::pair<bool, TD*>(false, &(root->data));
    }
};
