/**
 * @file RandGen.hpp
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy RandGen.
 */

#pragma once
#include <random>
#include <chrono>

/**
 * @brief Generator całkowitych liczb pseudolosowych.
 *
 * Używa wzorca singletona z opóźnioną (leniwą) inicjalizacją. Inicjalizacja następuje przy
 * pierwszym wywołaniu metody get(). Generuje liczby o rozkładzie jednostajnym.
 */
class RandGen
{
private:

    std::mt19937_64 mt; /**< Generator liczb pseudolosowych (Mersenne Twister Generator). */

    /** @brief Konstruktor domyślny.
     *
     * Inicjuje generator @p mt z pseudolosowym ziarnem. Wywoływany jest jednokrotnie przy
     * wywołaniu metody get().
     */
    RandGen()
    {
        std::chrono::high_resolution_clock::duration d;
        d = std::chrono::high_resolution_clock::now().time_since_epoch();
        mt.seed(d.count());
    }

    /** @brief Funkcja zwracająca generator jako singleton.
     *
     * Pierwsze wywołanie funkcji inicjalizuje generator przez wywołanie konstruktora.
     * @return Referencja do instancji generatora.
     */
    static RandGen& instance()
    {
        static RandGen rg;
        return rg;
    }

public:

    /** @brief Funkcja generująca całkowitą liczbę pseudolosową w danym przedziale.
     *
     * Zwraca całkowitą losową liczbę z przedziału [ @p min , @p max ] lub [ @p max , @p min ],
     * jeżeli @p min > @p max. Rozkład prawdopodobieństwa w danym przedziale jest jednostanjny.
     * Pierwsze wywołanie funkcji powoduje również zainicjowanie generatora @p mt.
     * @param min dolna granica przedziału, z którego ma zostać wybrana liczba.
     * @param max górna granica przedziału, z którego ma zostać wybrana liczba.
     * @return Wylosowana z danego przedziału liczba pseudolosowa.
     */
    template <typename T>
    static const T get(const T min, const T max)
    {
        RandGen& rg = RandGen::instance();
        if (min <= max)
        {
            std::uniform_int_distribution<T> dist(min, max);
            return dist(rg.mt);
        }
        else
        {
            std::uniform_int_distribution<T> dist(max, min);
            return dist(rg.mt);
        }
    }

    /** @brief Funkcja generująca rzeczywistą liczbę pseudolosową w danym przedziale.
     *
     * Zwraca rzeczywistą losową liczbę z przedziału [ @p min , @p max ] lub [ @p max , @p min ],
     * jeżeli @p min > @p max. Rozkład prawdopodobieństwa w danym przedziale jest jednostanjny.
     * Pierwsze wywołanie funkcji powoduje również zainicjowanie generatora @p mt.
     * @param min dolna granica przedziału, z którego ma zostać wybrana liczba.
     * @param max górna granica przedziału, z którego ma zostać wybrana liczba.
     * @return Wylosowana z danego przedziału liczba pseudolosowa.
     */
    template <typename T>
    static const T getReal(const T min, const T max)
    {
        RandGen& rg = RandGen::instance();
        if (min <= max)
        {
            std::uniform_real_distribution<T> dist(min, max);
            return dist(rg.mt);
        }
        else
        {
            std::uniform_real_distribution<T> dist(max, min);
            return dist(rg.mt);
        }
    }

    RandGen(RandGen const&) = delete;
    void operator=(RandGen const&) = delete;
};
