/**
 * @file Visitor.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy Visitor.h
 */

#pragma once

namespace Generator
{
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
}

namespace Game
{
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
}

namespace Solver
{
    class Pawn;
    class Bishop;
    class Rook;
    class Queen;
    class King;
}

/**
 * @brief Klasa implementująca wzorzec wizytatora.
 *
 * Pozwala na konwersję międzi różnymi hierarchiami figur bez konieczności dokładnej znajomości
 * konkretnych typów. Wszystkie implementacje funkcji wirtualnych są puste.
 */
class Visitor
{
public:

    virtual void visit(const Generator::Pawn& p){}
    virtual void visit(const Generator::Bishop& b){}
    virtual void visit(const Generator::Rook& r){}
    virtual void visit(const Generator::Queen& q){}
    virtual void visit(const Generator::King& k){}

    virtual void visit(const Game::Pawn& p){}
    virtual void visit(const Game::Bishop& b){}
    virtual void visit(const Game::Rook& r){}
    virtual void visit(const Game::Queen& q){}
    virtual void visit(const Game::King& k){}

    virtual void visit(const Solver::Pawn& p){}
    virtual void visit(const Solver::Bishop& b){}
    virtual void visit(const Solver::Rook& r){}
    virtual void visit(const Solver::Queen& q){}
    virtual void visit(const Solver::King& k){}

    virtual ~Visitor(){}
};
