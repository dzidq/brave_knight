/**
 * @file ExceptionQueueFull.h
 * @author Emil Bałdyga
 * @brief Plik zawierający definicję klasy ExceptionQueueFull.
 */

#pragma once
#include "Exception.h"

class ExceptionQueueFull: public Exception
{
public:

    virtual const char* what() const noexcept;
};
