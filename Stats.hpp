#pragma once
#include <cmath>
#include <vector>

template <typename T>
double mean(std::vector<T> vec)
{
    if (vec.empty())
        throw -1;
    double m = 0;
    for (const T& v: vec)
    {
        m += static_cast<double>(v);
    }
    return m/vec.size();
}

template <typename T>
double stddev(std::vector<T> vec)
{
    if (vec.size() == 1)
        return 0;
    double m = mean(vec);
    double d = 0;
    for (const T& v: vec)
    {
        d += pow(v-m, 2);
    }
    return sqrt(d/(vec.size()-1));
}
