#pragma once

#include "generator/Generator.h"
#include <vector>
#include <chrono>
#include <iostream>
#include "Stats.hpp"

void testGenerator(Generator::Parameters params, const unsigned iterations)
{
    std::vector<unsigned> gen_time(iterations, 0);

    for (unsigned i = 0; i < iterations; ++i)
    {
        params.bake();
        auto gen_start_time = std::chrono::system_clock::now();
        Generator::generate(params);
        auto gen_end_time = std::chrono::system_clock::now();
        gen_time[i] = std::chrono::duration_cast<std::chrono::milliseconds>(gen_end_time-gen_start_time).count();
    }

    std::cout << "Generation time: (avg) " << mean(gen_time) << " ms\t(dev) " << stddev(gen_time) << " ms\n";
}
